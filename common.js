var util = require('util')
var chai = require('chai');
var expect = chai.expect;
var request = require('supertest');
var Promise = require('promise');

/*
	Basic use of the testing framework I wrote is:
	1) Send a request with an "authenticator" function.
	2) Verify that the response was correct by promise chaining
	into a "check" function.

	Notes about authenticator functions:
		1) .pLogin and .pLogout are their own functions because
			they see so much use.
		2) All authenticator functions return promises except
			.cLogout and .cLogin, which use callbacks
		3) All four login/logout functions perform their own
			error checking internally, and are not intended to
			be promise chained into a check function.

	Notes about check functions:
		1) Non-login/logout authenticator functions always resolve their response payload.
			The check functions only have knowledge of the response payload
			if this response body is passed to them.
		2) Some check functions require a "checkBody" param.
			These functions are for when the response payload is expected
			to contain a "data" key/value pair.
		3) All check functions return promises.
		4) "check.permissions" is a special function, that tests one request
			across multiple users at once.
*/

/*
	For use with "check.permissions()". Supplies all the testing
	data necessary to test a request when executed by a specific user,
	except for the request itself.
	The request is passed in as a separate argument to check.permissions().
	Parameters:
		user		userName of a valid user. A null user tests
				a request without logging in.
		password	password associated with userName
		itString	description of test. Printed to the console
		expect		function that verifies contents of response payload.
				Return payload is delivered to expect as a promise
				resolution object.
				Must return a resolved or rejected promise.
				Intended to be a function from the check object
				declared in this file.
*/
class Test
{
	constructor(user, password, itString, expect)
	{
		this.user=user
		this.password=password
		this.itString=itString
		this.expect=expect
	}
}

//Uses chai to send requests. Somehow remembers
//login information between requests.
authenticator =
{
	user:chai.request.agent("http://icondev.icontrol.ws/api"),

	cLogin:function(userName, password, done, doPrint=false)
		{
			this.user
				.post('/users/' + userName)
				.send({'password':password})
				.end(function(err,res)
				{
					if(doPrint)
						console.error(userName + ' cLogin ' + res.body)
					expect(res.statusCode).to.equal(200)
					done()
				})
		},
	cLogout:function(userName, done, doPrint=false)
		{
			this.user
				.delete('/users/' + userName)
				.end(function(err,res)
				{
					if(doPrint)
						console.error(userName + ' cLogout ' + res.statusCode)
					expect(res.statusCode).to.equal(401)
					done();
				})
		},
	pLogin:function(userName, password, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				authenticator.user
				.post('/users/' + userName)
				.send({'password':password})
				.then(function(res)
				{
					if(doPrint)
						console.error(userName + ' pLogin ' + res.statusCode)
					expect(res.statusCode).to.equal(200)
					resolve(res)
				})
				.catch(function(error){reject(error)})
			})
		},
	pLogout:function(userName, doPrint=false)
		{
			return new Promise(function(resolve,reject)
			{
				authenticator.user
				.delete('/users/' + userName)
				.then(function(res)
				{
					if(doPrint)
						console.error(userName + " pLogout " + res.statusCode)
					expect(res.statusCode).to.equal(401)
					resolve(res)
				})
				.catch(function(error){reject(error)})
			})
		},

	get:function(url, reqBody=null, queryBody=null, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				authenticator.user
				.get(url)
				.send(reqBody)
				.query(queryBody)
				.then(function(res)
				{
					if(doPrint)
						console.error(util.inspect(res.body, false, null, true))
					resolve(res)
				})
			})
		},
	post:function(url, reqBody=null, queryBody=null, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				authenticator.user
				.post(url)
				.send(reqBody)
				.query(queryBody)
				.then(function(res)
				{
					if(doPrint)
						console.error(util.inspect(res.body, false, null, true))
					resolve(res)
				})
			})
		},
	put:function(url, reqBody=null, queryBody=null, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				authenticator.user
				.put(url)
				.send(reqBody)
				.query(queryBody)
				.then(function(res)
				{
					if(doPrint)
						console.error(util.inspect(res.body, false, null, true))
					resolve(res)
				})
			})
		},
	delete:function(url, reqBody=null, queryBody=null, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				authenticator.user
				.delete(url)
				.send(reqBody)
				.query(queryBody)
				.then(function(res)
				{
					if(doPrint)
						console.error(util.inspect(res.body, false, null, true))
					resolve(res)
				})
			})
		},
}

/*
	The intended use of these functions is to be promised chained after
	a request. In other words, authenticator.get, .post, .delete, and .put
	resolve a 'res' object. Passing that object to these 'check' functions
	verifies the contents.

	Tests the contents of a response payload.
	Naming convention is <IC return code>_['DNE', 'Contains', 'Eqls'],
	where DNE, Contains, or Eqls refers to whether a 'data' payload
	is expected to not exist, to contain 'checkBody', or to equal 'checkBody'
	respectively.
*/
check =
{
	errorTemplate:function(res, httpCode, icCode, doPrint=false)
		{
			//If the tomcat detects an error before making the ic call
			//to the database, then the tomcat will return the httpCode
			//a second time, instead of returning the icCode, because
			//there is no icCode to return
			if(icCode == null)
				icCode = httpCode
			return new Promise(function(resolve, reject)
			{
				if(doPrint)
					console.error(util.inspect(res.body, false, null, true))
				try {
					expect(res.statusCode).to.equal(httpCode)
					expect(res.body).to.have.all.keys('errors')
					expect(res.body).to.not.have.any.keys('info', 'warnings', 'data')
					expect(res.body.errors[0]).to.include({'code':icCode.toString()})
					resolve(res)
				} catch(error){reject(error)}
			})
		},
	//"dne" stands for "does not exist", as in the 'data' portion of the
	//returned json object does not exist.
	dneTemplate:function(res, httpCode, icCode, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				if(doPrint)
					console.error(util.inspect(res.body, false, null, true))
				try {
					expect(res.statusCode).to.equal(httpCode)
					expect(res.body).to.have.all.keys('info')
					expect(res.body).to.not.have.any.keys('errors', 'warnings', 'data')
					expect(res.body.info[0]).to.include({'code':icCode.toString()})
					resolve(res)
				} catch(error){reject(error)}
			})
		},
	//"containSubset" only checks that the returned data json object (res.body.data)
	//is a superset of the expected json object (checkBody)
	containsTemplate:function(res, checkBody, httpCode, icCode, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				if(doPrint)
					console.error(util.inspect(res.body, false, null, true))
				try {
					expect(res.statusCode).to.equal(httpCode)
					expect(res.body).to.have.all.keys('info', 'data')
					expect(res.body).to.not.have.any.keys('errors', 'warnings')
					expect(res.body.info[0]).to.include({'code':icCode.toString()})
					expect(res.body.data).to.containSubset(checkBody)
					resolve(res)
				} catch(error){reject(error)}
			})
		},
	//"eql" checks that the returned data json object (res.body.data) is exactly
	//equal to the expected jsonobject (checkBody).
	eqlsTemplate:function(res, checkBody, httpCode, icCode, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				if(doPrint)
					console.error(util.inspect(res.body, false, null, true))
				try {
					expect(res.statusCode).to.equal(httpCode)
					expect(res.body).to.have.all.keys('info', 'data')
					expect(res.body).to.not.have.any.keys('errors', 'warnings')
					expect(res.body.info[0]).to.include({'code':icCode.toString()})
					expect(res.body.data).to.eql(checkBody)
					resolve(res)
				} catch(error){reject(error)}
			})
		},

	hasAllKeysTemplate:function(res, checkBody, httpCode, icCode, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				if(doPrint)
					console.error(util.inspect(res.body, false, null, true))
				try {
					expect(res.statusCode).to.equal(httpCode)
					expect(res.body).to.have.all.keys('info', 'data')
					expect(res.body).to.not.have.any.keys('errors', 'warnings')
					expect(res.body.info[0]).to.include({'code':icCode.toString()})
					expect(res.body.data).to.have.all.keys(checkBody)
					resolve(res)
				} catch(error){reject(error)}
			})
		},

	//TODO: read map values directly from errorMap.txt instead of having them hard coded in two separate places
	CREATED_DNE:function(res, doPrint=false)
		{return this.dneTemplate(res, 201, 62, doPrint)},

	CREATED_Contains:function(res, checkBody, doPrint=false)
		{return this.containsTemplate(res, checkBody, 201, 62, doPrint)},

	CREATED_Eqls:function(res, checkBody, doPrint=false)
		{return this.eqlsTemplate(res, checkBody, 201, 62, doPrint)},

	OK_DNE:function(res, doPrint=false)
		{return this.dneTemplate(res, 200, 0, doPrint)},

	OK_Contains:function(res, checkBody, doPrint=false)
		{return this.containsTemplate(res, checkBody, 200, 0, doPrint)},

	OK_Eqls:function(res, checkBody, doPrint=false)
		{return this.eqlsTemplate(res, checkBody, 200, 0, doPrint)},

	OK_HasKeys:function(res, checkBody, doPrint=false)
		{return this.hasAllKeysTemplate(res, checkBody, 200, 0, doPrint)},

	OK_NO_ENTRIES_RETURNED:function(res, doPrint=false)
		{return this.eqlsTemplate(res, {}, 200, 50, doPrint)},

	API_INVALID_ARGS:function(res, doPrint=false)
		{return this.errorTemplate(res, 400, 598, doPrint)},

	AUTHENTICATION_FAILED(res, doPrint=false)
		{return this.errorTemplate(res, 403, 599, doPrint)},

	BAD_REQUEST:function(res, doPrint=false)
		{return this.errorTemplate(res, 400, null, doPrint)},

	CONFLICT:function(res, doPrint=false)
		{return this.errorTemplate(res, 409, null, doPrint)},

	DEVICE_NO_ACCESS:function(res, doPrint=false)
		{return this.errorTemplate(res, 403, 533, doPrint)},

	DEVICE_NOT_FOUND:function(res, doPrint=false)
		{return this.errorTemplate(res, 409, 532, doPrint)},

	DUPLICATE_NAME:function(res, doPrint=false)
		{return this.errorTemplate(res, 409, 612, doPrint)},

	FILE_NOT_FOUND:function(res, doPrint=false)
		{return this.errorTemplate(res, 409, 576, doPrint)},

	FORBIDDEN:function(res, doPrint=false)
		{return this.errorTemplate(res, 403, null, doPrint)},

	INTERNAL_SERVER_ERROR:function(res, doPrint=false)
		{return this.errorTemplate(res, 500, null, doPrint)},

	NO_PRIVILEGES:function(res, doPrint=false)
		{return this.errorTemplate(res, 403, 600, doPrint)},

	NO_SUCH_ITEM:function(res, doPrint=false)
		{return this.errorTemplate(res, 409, 613, doPrint)},

	SQL_ERROR:function(res, doPrint=false)
		{return this.errorTemplate(res, 500, 526, doPrint)},

	TABLE_NO_ACCESS:function(res, doPrint=false)
		{return this.errorTemplate(res, 403, 706, doPrint)},

	UNAUTHORIZED:function(res, doPrint=false)
		{return this.errorTemplate(res, 401, null, doPrint)},

	UNKNOWN_TABLE:function(res, doPrint=false)
		{return this.errorTemplate(res, 400, 705, doPrint)},

	/*
		Highly specific: probably a temporary solution.
		Checks that a request returned 201 CREATED
		with a data section that includes only an 'entryId' key,
		with a value consisting of digits
	*/
	CREATED_ENTRYID:function(res, doPrint=false)
		{
			return new Promise(function(resolve, reject)
			{
				if(doPrint)
					console.error(res.body)
				try {
					expect(res.statusCode).to.equal(201)
					expect(res.body).to.have.all.keys('info', 'data')
					expect(res.body).to.not.have.any.keys('errors', 'warnings')
					expect(res.body.info[0]).to.include({'code':'62'})
					expect(res.body.data).to.have.all.keys('entryId')
					expect(res.body.data['entryId']).to.match(/\d+/)
					resolve(res)
				} catch(error){reject(error)}
			})
		},
}
/*
authenticator.pLogout = (function(userName)
{
	return new Promise(function(resolve,reject)
	{
		authenticator.user
		.delete('/users/' + userName)
		.then(function(res)
		{
			//console.error(userName + " pLogout " + res.statusCode)
			resolve(res)
		})
	})
	.then(function()
	{
		return new Promise(function(resolve,reject)
		{
			authenticator.user.close()
			resolve()
		})
	})
})
*/


/*
	Runs tests specified by each test object in the array.
		Request		a non-login/logout authenticator function to be tested
		testArray	array of Test objects (declared at the top of this file)
*/
check.permissions = function(request, testArray)
{
	function runTest(k)
	{
		it(testArray[k].itString, function()
		{
			return new Promise(function(resolve, reject)
			{
				if(testArray[k].user == null)
					resolve()
				else
				{
					return authenticator.pLogin(testArray[k].user, testArray[k].password)
					.then(function(){resolve()})
					.catch(function(error){reject(error)})
				}
			})
			.then(function()
			{
				return request()
				.then(function(res){return testArray[k].expect(res)})
				.finally(function()
				{
					if(testArray[k].user == null)
						return Promise.resolve()
					else
						return authenticator.pLogout(testArray[k].user)
				})
			})
		})
	}

	for(var k = 0; k < testArray.length; k++)
		runTest(k)
}

/*
	Single user version of above function.
	Does not generate its own it block (intended to be called from within a pre-existing it block)
*/
check.permission = function(request, userName, password, expect)
{
	return new Promise(function(resolve, reject)
	{
		if(userName == null)
			resolve()
		else
		{
			return authenticator.pLogin(userName, password)
			.then(function(){resolve()})
			.catch(function(error){reject(error)})
		}
	})
	.then(function()
	{
		return request()
		.then(function(res){return expect(res)})
		.finally(function(){return authenticator.pLogout(userName)})
	})
}
module.exports = {authenticator:authenticator, check:check, Test:Test};

var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//constants for testing this function.
var getTableColumnInfoURL = function(tableName){return '/tables/' + tableName}

/*
	Sample tables from each privilege type.
		As of Steve's 8/22/18 email, there are no
		tables that designers and provisioners can read that
		aren't available to the general public.
*/
const publicTable = 'shippingPickList'
const provisionerAccess = null
const designerAccess = null
const cmpAdminTable = 'serverConfig'
const adminTable = 'control'
const nonExistantTable='qwertyuiopzxzx'

describe('get Table Column Info with various privileges', function()
{
	var testPublicArray =
	[
		new Test(null, null, 'getting table column info without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting public table column info with public privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
		new Test(provisioner1, password, 'getting public table column info with provisioner privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
		new Test(designer1, password, 'getting public table column info with designer privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
		new Test(cmpAdmin1, password, 'getting public table column info with company Admin privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
	]
	var testCmpAdminArray =
	[
		new Test(public1, password, 'getting company Admin table column info with public privileges. Should fail with FORBIDDEN',
			function(res){return check.TABLE_NO_ACCESS(res)}),
		new Test(provisioner1, password, 'getting company Admin table column info with provisioner privileges. Should fail with FORBIDDEN',
			function(res){return check.TABLE_NO_ACCESS(res)}),
		new Test(designer1, password, 'getting company Admin table column info with designer privileges. Should fail with FORBIDDEN',
			function(res){return check.TABLE_NO_ACCESS(res)}),
		new Test(cmpAdmin1, password, 'getting company Admin table column info with company Admin privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
	]
	var testAdminArray =
	[
		new Test(public1, password, 'getting Admin table column info with public privileges. Should fail with FORBIDDEN',
			function(res){return check.TABLE_NO_ACCESS(res)}),
		new Test(provisioner1, password, 'getting Admin table column info with provisioner privileges. Should fail with FORBIDDEN',
			function(res){return check.TABLE_NO_ACCESS(res)}),
		new Test(designer1, password, 'getting Admin table column info with designer privileges. Should fail with FORBIDDEN',
			function(res){return check.TABLE_NO_ACCESS(res)}),
		new Test(cmpAdmin1, password, 'getting Admin table column info with company Admin privileges. Should fail with FORBIDDEN',
			function(res){return check.TABLE_NO_ACCESS(res)}),
	]
	check.permissions(function(res){return authenticator.get(getTableColumnInfoURL(publicTable))}, testPublicArray)
	check.permissions(function(res){return authenticator.get(getTableColumnInfoURL(cmpAdminTable))}, testCmpAdminArray)
	check.permissions(function(res){return authenticator.get(getTableColumnInfoURL(adminTable))}, testAdminArray)
})

describe('get table column info with various bad parameters', function()
{
	it('get a non-existant table. Should fail with CONFLICT', function()
	{
		return authenticator.pLogin(cmpAdmin1, password)
		.then(function()
		{
			return authenticator.get(getTableColumnInfoURL(nonExistantTable))
			.then(function(res){return check.UNKNOWN_TABLE(res)})
			.finally(function(){return authenticator.pLogout(cmpAdmin1)})
		})
	})
})

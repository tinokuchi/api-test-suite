//No mapping for 576 ERR_FILE_NOT_FOUND. Suggest 409 CONFLICT.
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//variables for testing
const genFile = 'TestEnigmaGPSCell.txt'
var getFileURL = function(genFile){return '/commands/files/' + genFile}
const checkBody =
{
	"contents":
	[
		'15 "T" 200',
		'253 2 255 255 255 255 255 132 0 97 31 1',
		'253 3 255 255 255 255 255 148 50 97 33 1',
		'27 \'T\' 64 71 27 235 0xAB 0xDE "c1.korem2m.com"',
		'240',
		'15 84 5'
	]
}


describe('get command file with various privileges', function()
{
	var testArray =
	[
		new Test(null, null, 'getting command file without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting command file with public privileges. Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res)}),
		new Test(provisioner1, password, 'getting command file with provisioner privileges. Should contents of command file',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(designer1, password, 'getting command file with designer privileges. Should return contents of command file',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(cmpAdmin1, password, 'getting command file with company Admin privileges. Should return contents of command file',
			function(res){return check.OK_Eqls(res, checkBody)}),
	]
	return check.permissions(function(res){return authenticator.get(getFileURL(genFile))}, testArray)
})

describe('get a command file with various bad parameters', function()
{
	badGenFile = 'asdf'
	before(function(){return authenticator.pLogin(cmpAdmin1, password)})
	after(function(){return authenticator.pLogout(cmpAdmin1)})
	it("get a non-existant command file", function()
	{
		return authenticator.get(getFileURL(badGenFile))
		.then(function(res){return check.FILE_NOT_FOUND(res)})
	})
})

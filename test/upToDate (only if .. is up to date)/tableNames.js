var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//constants for testing this function.
const getTableNamesURL = '/tables'

/*
	provisioner, designer, cmpAdmin, and adminOnly
	access tables lists described here are
	exhaustive as of Steve's email 8/29/18
*/
/*
	publicAccess is not exhaustive.
	publicAccess and adminOnly are mutually
	exclusive with the other four tables.

	Currently makes a lot of comparisons,
	necessary because publicAccess is not
	exhaustive.
*/
const publicAccess = ['shippingPickList']
const provisionerAccess = []
const designerAccess = []
const cmpAdminAccess = ['dsoConfig', 'dsoMessage', 'serverConfig']
const adminOnly = ['control', 'deviceKey', 'license']

//Functions
/*
	includeArray and excludeArray must be mutually exclusive
	for this test to function properly
*/
checkAccessTemplate = function(res, includeArray, excludeArray, doPrint=false)
{
	return new Promise(function(resolve, reject)
	{
		if(doPrint)
			console.error(res.body)
		try {
			expect(res.statusCode).to.equal(200)
			expect(res.body).to.have.all.keys('info', 'data')
			expect(res.body).to.not.have.any.keys('errors', 'warnings')
			expect(res.body.info[0]).to.include({'code':'0'})
			expect(res.body.data).to.include.members(includeArray)
			expect(res.body.data).to.not.include.members(excludeArray)
			resolve(res)
		} catch(error){reject(error)}
	})
}

/*
	Filters all elements found in includeArray out of excludeRawArray,
	and returns the result.
*/
makeExclusive = function(includeArray, excludeRawArray)
{
	return excludeRawArray.filter(function(word){return !includeArray.includes(word)})
}

describe('get table names with various privileges', function()
{
	checkPublicAccess = function(res, doPrint=false)
	{
		var includeArray = publicAccess
		var excludeArray = makeExclusive(includeArray, provisionerAccess.concat(designerAccess.concat(cmpAdminAccess.concat(adminOnly))))
		return checkAccessTemplate(res, includeArray, excludeArray, doPrint)
	}
	checkProvisionerAccess = function(res, doPrint=false)
	{
		var includeArray = publicAccess.concat(provisionerAccess)
		var excludeArray = makeExclusive(includeArray, designerAccess.concat(cmpAdminAccess.concat(adminOnly)))
		return checkAccessTemplate(res, includeArray, excludeArray, doPrint)
	}
	checkDesignerAccess = function(res, doPrint=false)
	{
		var includeArray = publicAccess.concat(designerAccess)
		var excludeArray = makeExclusive(includeArray, provisionerAccess.concat(cmpAdminAccess.concat(adminOnly)))
		return checkAccessTemplate(res, includeArray, excludeArray, doPrint)
	}
	checkCmpAdminAccess = function(res, doPrint=false)
	{
		var includeArray = publicAccess.concat(cmpAdminAccess)
		var excludeArray = makeExclusive(includeArray, provisionerAccess.concat(designerAccess.concat(adminOnly)))
		return checkAccessTemplate(res, includeArray, excludeArray, doPrint)
	}
	var testArray =
	[
		new Test(null, null, 'getting tables names without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting tables names with public privileges. Should return public tables',
			function(res){return checkPublicAccess(res)}),
		new Test(provisioner1, password, 'getting tables names with provisioner privileges. Should return public and provisioner tables',
			function(res){return checkProvisionerAccess(res)}),
		new Test(designer1, password, 'getting tables names with designer privileges. Should return public and designer tables',
			function(res){return checkDesignerAccess(res)}),
		new Test(cmpAdmin1, password, 'getting tables names with company Admin privileges. Should return public and company Admin tables',
			function(res){return checkCmpAdminAccess(res)}),
	]
	return check.permissions(function(res){return authenticator.get(getTableNamesURL)}, testArray)
})

var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//Constants for testing this function
const getUserStatusURL = '/users'
const numUsersInCompany1 = 4
const numUsersInCompany3 = 4

//TODO: change once payload for this request is standardized.
checkCompanyAdmin = function(res, userName, numUsersInCompany, doPrint=false)
{
	return check.OK_Contains(res, {}, doPrint)
	.then(function(res)
	{
		try {
			expect(res.body.data).to.have.all.keys('username', 'userInfo')
			expect(res.body.data.username).to.equal(userName)
			expect(res.body.data.userInfo).to.have.lengthOf(numUsersInCompany)
			return Promise.resolve(res)
		} catch(error){return Promise.reject(error)}
	})
}

//TODO: change once payload for this request is standardized.
checkOtherUser = function(res, userName, doPrint=false)
{
	return check.OK_Contains(res, {}, doPrint)
	.then(function(res)
	{
		try {
			expect(res.body.data).to.have.all.keys('username', 'userInfo')
			expect(res.body.data.username).to.equal(userName)
			expect(res.body.data.userInfo).to.have.lengthOf(1)
			expect(res.body.data.userInfo[0].fullName).to.equal(userName)
			return Promise.resolve(res)
		} catch(error){return Promise.reject(error)}
	})
}

describe('getUserStatus with various privileges', function()
{
	var testArray =
	[
		new Test(null, null, 'getting a map set without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting user status with public privileges. Should only return info on the current user',
			function(res){return checkOtherUser(res, public1)}),
		new Test(public3, password, 'getting user status with public privileges from a parent company. Should only return info on the current user',
			function(res){return checkOtherUser(res, public3)}),
		new Test(provisioner1, password, 'getting user status with provisioner privileges. Should only return info on the current user',
			function(res){return checkOtherUser(res, provisioner1)}),
		new Test(provisioner3, password, 'getting user status with provisioner privileges from a parent company. Should only return info on the current user',
			function(res){return checkOtherUser(res, provisioner3)}),
		new Test(designer1, password, 'getting user status with designer privileges. Should only return info on the current user',
			function(res){return checkOtherUser(res, designer1)}),
		new Test(designer3, password, 'getting user status with designer privileges from a parent company. Should only return info on the current user',
			function(res){return checkOtherUser(res, designer3)}),
		new Test(cmpAdmin1, password, 'getting user status with company Admin privileges. Should only return info on the current company',
			function(res){return checkCompanyAdmin(res, cmpAdmin1, numUsersInCompany1)}),
		new Test(cmpAdmin3, password, 'getting user status with company Admin privileges from a parent company. Should return info on both the current and child companies',
			function(res){return checkCompanyAdmin(res, cmpAdmin3, numUsersInCompany3 + numUsersInCompany1)}),
	]

	check.permissions(function(){return authenticator.get(getUserStatusURL)}, testArray)
})

//TODO: add company level error checking to the java and add the appropriate tests
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const companyId1 = '111'
const company2 = 'testCompany2'
const companyId2 = '112'
const company3 = 'testCompany3'
const companyId3 = '113'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'


//variables and functions for testing this request
const executeSQLQueryPutURL = "/tables/query/"

const tableName = 'shippingPickList'
const queryName = 'testQuery'
const guid1 = '01AC6F49-8F0D-4154-BA2A-8421BB8F1B90'
const guid2 = '8CDF7AB5-987B-4178-B7AA-49C4B27F377A'
const forbiddenGuid = 'EFDDF3E8-73EB-411F-BBB5-4052997C36BB'
const nonExistantTableName = 'nonExistantTableName'
const nonGuidTableName = 'shippingRouteMapAssociations'
const nonExistantQueryName = 'nonExistantQueryName'

const entryGuid1 = '01AC6F49-8F0D-4154-BA2A-8421BB8F1B90'
const entryName1 = 'Job 09F85DE8 - Created Mar-19-2018'
const entryId1 = '2196'

const entryGuid2 = '8CDF7AB5-987B-4178-B7AA-49C4B27F377A'
const entryName2 = 'Job 09C9085A - Updated Feb-13-2018'
const entryId2 = '2195'

const illegalCharacters = "?!"

var putBody = function(guid){return {"name":queryName, "params":JSON.stringify([guid])}}
const checkBody1 =
{
	"0":
	{
		"name": entryName1,
		"companyId": companyId1,
		"entryId": entryId1,
	}
}
const checkBody2 =
{
	"0":
	{
		"name": entryName2,
		"companyId": companyId2,
		"entryId": entryId2,
	}
}


describe("execute test query", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('Should return a table entry', function()
	{
		return authenticator.post(executeSQLQueryPutURL, putBody(guid1))
		.then(function(res){return check.OK_Eqls(res, checkBody1)})
	})
})

describe("execute test query with various privileges", function()
{
	var testArray =
	[
		new Test(null, null, 'executing shipment table without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'executing shipment table with public privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody1)}),
		new Test(provisioner1, password, 'executing shipment table with provisioner privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody1)}),
		new Test(designer1, password, 'executing shipment table with designer privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody1)}),
		new Test(cmpAdmin1, password, 'executing shipment table with company Admin privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody1)}),
	]
	check.permissions(function(){return authenticator.post(executeSQLQueryPutURL, putBody(guid1))}, testArray)
})

describe("execute test query with guid that does not belong to a test company", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('Should succeed, but return nothing', function()
	{
		return authenticator.post(executeSQLQueryPutURL, putBody(forbiddenGuid))
		.then(function(res){return check.OK_NO_ENTRIES_RETURNED(res)})
	})
})

describe("execute test query with various bad parameters", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})

	it("Missing query name. Should fail with BAD REQUEST", function()
	{
		tempBody = {"params":JSON.stringify([guid1])}
		return authenticator.post(executeSQLQueryPutURL, tempBody)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("illegal character in query parameter array. Should fail with FORBIDDEN", function()
	{
		tempBody = {"name":queryName, "params":JSON.stringify([illegalCharacters])}
		return authenticator.post(executeSQLQueryPutURL, tempBody)
		.then(function(res){return check.FORBIDDEN(res)})
	})
	it("illegal character in query parameter array sub-array. Should fail with FORBIDDEN", function()
	{
		tempBody = {"name":queryName, "params":JSON.stringify([[illegalCharacters]])}
		return authenticator.post(executeSQLQueryPutURL, tempBody)
		.then(function(res){return check.FORBIDDEN(res)})
	})
	it("more arguments to query than parameters in that query. Should fail with BAD_REQUEST", function()
	{
		tempBody = {"name":queryName, "params":JSON.stringify([guid1, guid2])}
		return authenticator.post(executeSQLQueryPutURL, tempBody)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("less arguments to query than parameters in that query. Should fail with BAD_REQUEST", function()
	{
		tempBody = {"name":queryName, "params":JSON.stringify([])}
		return authenticator.post(executeSQLQueryPutURL, tempBody)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("non-existant queryname. Should fail with CONFLICT", function()
	{
		tempBody = {"name":nonExistantQueryName, "params":JSON.stringify([])}
		return authenticator.post(executeSQLQueryPutURL, tempBody)
		.then(function(res){return check.CONFLICT(res)})
	})
})

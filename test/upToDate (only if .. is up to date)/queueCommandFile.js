//Prints 0 RETURN_OK as the first status whenever it would print 533 ERR_DEVICE_NO_ACCESS
//only owning designers and company admins can load to a device.
//on 576 ERR_FILE_NOT_FOUND, the getText portion reads: 'Specified file does not exist (%1)', with the (%1) still showing
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//constants and functions for testing this request.
const testFile = "testCommand.txt"
const testCommand = "010505"
const macID = '00_1B_AD_00_EC_0C_35_40'
const macID2 = '00_1B_AD_00_EC_0C_35_00'
const queueCommandFileURL = '/commands/queuefile'
removeFromQueueURL = function(macID){return '/commands/queue/' + macID}
getQueueURL = function(macID){return '/commands/queue/' + macID}

loadBody =
{
	"device":macID,
	"file":testFile
}
removeBody =
{
	"sequenceIds":[]
}
/*
	Returns a bunch of information that is assigned by the server.
	Thus, I can't know the contents of the return payload at compile time.
	The most I know here is what keys should be in the return payload.
*/
checkQueue = function(res, doPrint=false)
{
	return check.OK_Contains(res, {}, doPrint)
	.then(function(res)
	{
		try {
			for(var k = 0; k.toString() in res.body.data; k++)
			{
				expect(res.body.data).to.include.all.keys(k.toString())
				expect(res.body.data[k.toString()]).to.have.all.keys('macId', 'entryId', 'createDate', 'queuedBy', 'sequenceId', 'data')
				expect(res.body.data[k.toString()]["macId"]).to.equal(macID)
				expect(res.body.data[k.toString()]["data"]).to.equal(testCommand)
			}
			return Promise.resolve(res)
		} catch(error){return Promise.reject(error)}
	})
}
/*
	Removing a command requires knowledge of the command's sequence id,
	which is assigned by the server. Thus, I need to first call getQueue()
	to find out what sequence Ids the server has assigned.
*/
fillRemoveBody = function(macID, removeBody, doPrint=false)
{
	return authenticator.get(getQueueURL(macID))
	.then(function(res){return checkQueue(res, doPrint)})
	.then(function(res)
	{
		removeBody['sequenceIds'] = []
		for(var k = 0; k.toString() in res.body.data; k++)
			removeBody['sequenceIds'].push(res.body.data[k.toString()].sequenceId)
		return Promise.resolve(res)
	})
}
clearQueue = function(macID, doPrint=false)
{
	return fillRemoveBody(macID, removeBody, doPrint)
	.then(function(res)
	{
		return authenticator.delete(removeFromQueueURL(macID), removeBody)
		.then(function(res){return check.OK_DNE})
		.finally(function()
		{
			removeBody['sequenceIds'] = []
			return Promise.resolve(res)
		})
	})
}
checkQueueLength = function(res, max, doPrint=false)
{
	return checkQueue(res, doPrint)
	.then(function(res)
	{
		var k
		var keysArray = []
		for(k = 0; k < max; k++)
			keysArray.push(k.toString())
		if(keysArray.length == 0)
			expect(res.body.data).to.eql({})
		else
		{
			expect(res.body.data).to.have.all.keys(keysArray)
			expect(res.body.data).to.not.have.any.keys(k.toString())
		}
		return Promise.resolve(res)
	})
	.catch(function(error){return Promise.reject(error)})
}

/*
describe('load a command file', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function()
	{
		return clearQueue(macID)
		.finally(function(){return authenticator.pLogout(designer1)})
	})
	it('should succeed', function()
	{
		return authenticator.put(queueCommandFileURL, loadBody)
		.then(function(res){return check.OK_DNE(res)})
		.then(function(){return authenticator.get(getQueueURL(macID))})
		.then(function(res){return checkQueueLength(res, 1, true)})
	})
})

describe('load a command file with various privileges,', function()
{
	deviceNoAccessWrapper = function(res, doPrint=false)
	{
		return check.DEVICE_NO_ACCESS(res, doPrint)
		.catch(function(error)
		{
			return clearQueue(macID)
			.then(function(){return Promise.reject(error)})
		})
	}
	okDNEWrapper = function(res, doPrint=false)
	{
		return check.OK_DNE(res, doPrint)
		.then(function(){return clearQueue(macID)})
	}
	var testArray =
	[
		new Test(null, null, 'loading commands without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'loading commands with public privileges. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(public2, password, 'loading commands with public privileges from another company. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(public3, password, 'loading commands with public privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(provisioner1, password, 'loading commands with provisioner privileges. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(provisioner2, password, 'loading commands with provisioner privileges from another company. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(provisioner3, password, 'loading commands with provisioner privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(designer1, password, 'loading commands with designer privileges. Should succeed',
			function(res){return okDNEWrapper(res)}),
		new Test(designer2, password, 'loading commands with designer privileges from another company. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(designer3, password, 'loading commands with designer privileges from a parent company. Should succeed',
			function(res){return okDNEWrapper(res)}),
		new Test(cmpAdmin1, password, 'loading commands with company Admin privileges. Should succeed',
			function(res){return okDNEWrapper(res)}),
		new Test(cmpAdmin2, password, 'loading commands with company Admin privileges from another company. Should fail with FORBIDDEN',
			function(res){return deviceNoAccessWrapper(res)}),
		new Test(cmpAdmin3, password, 'loading commands with company Admin privileges from a parent company. Should succeed',
			function(res){return okDNEWrapper(res)}),
	]
	check.permissions(function(){return authenticator.put(queueCommandFileURL, loadBody)}, testArray)
})
*/
describe('load a command file with various bad parameters', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('load a command file with a missing parameter. Should fail with BAD_REQUEST', function()
	{
		tempBody = {"device":macID}
		return authenticator.put(queueCommandFileURL, tempBody)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it('load a command with a non-String parameter. Should fail with BAD_REQUEST', function()
	{
		tempBody = {"device":macID, "file":123}
		return authenticator.put(queueCommandFileURL, tempBody)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it('load a command with a non-existant macID. Should fail with FORBIDDEN', function()
	{
		tempBody = {"device":"this macID does not exist", "file":testFile}
		return authenticator.put(queueCommandFileURL, tempBody)
		.then(function(res){return check.DEVICE_NO_ACCESS(res)})
	})
	it('load a command with a non-existant test file. Should fail with CONFLICT', function()
	{
		tempBody = {"device":macID, "file":"this file does not exist"}
		return authenticator.put(queueCommandFileURL, tempBody)
		.then(function(res){return check.FILE_NOT_FOUND(res)})
	})
})

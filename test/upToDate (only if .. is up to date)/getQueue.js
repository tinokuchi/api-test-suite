
//company admins cannot get command queue
//parent company, any user cannot get command queue
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//constants for testing this function.
const macID = '00_1B_AD_00_EC_0C_35_40'
const macID2 = '00_1B_AD_00_EC_0C_35_00'
const loadToQueueURL = '/commands/queue'
removeFromQueueURL = function(macID){return '/commands/queue/' + macID}
getQueueURL = function(macID){return '/commands/queue/' + macID}

/*
	The most basic of load requests. If I need a more specific load, I will
	make a local variable.
*/
const loadBody =
{
	"deviceIdList":[macID],
	"commandList":['1 5 5']
}
/*
	declared here as a global variable and then manipulated by everyone. Maybe not the best idea.
	Different from loadBody because "sequenceIds" can't be known at compile time.
*/
removeBody =
{
	"sequenceIds":[]
}
/*
	Returns a bunch of information that is assigned by the server.
	Thus, I can't know the contents of the return payload at compile time.
	The most I know here is what keys should be in the return payload.
*/
checkQueue = function(res, doPrint=false)
{
	return new Promise(function(resolve, reject)
	{
		if(doPrint)
			console.error(res.body)
		try {
			for(var k = 0; k.toString() in res.body.data; k++)
			{
				expect(res.body.data).to.include.all.keys(k.toString())
				expect(res.body.data[k.toString()]).to.have.all.keys('macId', 'entryId', 'createDate', 'queuedBy', 'sequenceId', 'data')
			}
			resolve(res)
		} catch(error){reject(error)}
	})
}
/*
	Removing a command requires knowledge of the command's sequence id,
	which is assigned by the server. Thus, I need to first call getQueue()
	to find out what sequence Ids the server has assigned.
*/
fillRemoveBody = function(macID, removeBody, doPrint=false)
{
	return authenticator.get(getQueueURL(macID))
	.then(function(res){return checkQueue(res, doPrint)})
	.then(function(res)
	{
		removeBody['sequenceIds'] = []
		for(var k = 0; k.toString() in res.body.data; k++)
			removeBody['sequenceIds'].push(res.body.data[k.toString()].sequenceId)
		return Promise.resolve(res)
	})
}
clearQueue = function(macID, doPrint=false)
{
	return fillRemoveBody(macID, removeBody, doPrint)
	.then(function(res)
	{
		return authenticator.delete(removeFromQueueURL(macID), removeBody)
		.then(function(res){return check.OK_DNE})
		.finally(function()
		{
			removeBody['sequenceIds'] = []
			return Promise.resolve(res)
		})
	})
}
checkQueueLength = function(res, max, doPrint=false)
{
	return checkQueue(res, doPrint)
	.then(function(res)
	{
		var k
		var keysArray = []
		for(k = 0; k < max; k++)
			keysArray.push(k.toString())
		if(keysArray.length == 0)
			expect(res.body.data).to.eql({})
		else
		{
			expect(res.body.data).to.have.all.keys(keysArray)
			expect(res.body.data).to.not.have.any.keys(k.toString())
		}
		return Promise.resolve(res)
	})
	.catch(function(error){return Promise.reject(error)})
}



describe('get device commands from a device with an empty command queue', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("Should succeed with an empty data payload", function()
	{
		return authenticator.get(getQueueURL(macID))
		.then(function(res){return check.OK_Eqls(res, {})})
	})
})

describe('get device commands from a device with a non-empty command queue', function()
{
	before(function()
	{
		tempLoadBody =
		{
			"deviceIdList":[macID],
			"commandList":['1 5 5', '1 5 5']
		}
		return authenticator.pLogin(designer1, password)
		.then(function(res){return authenticator.put(loadToQueueURL, tempLoadBody)})
		.then(function(res){return check.OK_DNE(res)})

	})
	after(function()
	{
		return clearQueue(macID)
		.finally(function(){return authenticator.pLogout(designer1)})
	})
	it("Should succeed with expected contents in data payload", function()
	{
		return authenticator.get(getQueueURL(macID))
		.then(function(res){return check.OK_Contains(res, {})})
		.then(function(res){return checkQueueLength(res, 2)})
	})
})

describe('get device commands with various privileges', function()
{
	checkOkWrapper = function(res, doPrint=false)
	{
		return check.OK_Contains(res, {}, doPrint)
		.then(function(){return checkQueueLength(res, 1)})
	}
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.put(loadToQueueURL, loadBody)
			.then(function(res){return check.OK_DNE(res)})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	after(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return clearQueue(macID)
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	var testArray =
	[
		new Test(null, null, 'getting commands without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting commands with public privileges. Should fail with FORBIDDEN',
			function(res){return check.DEVICE_NO_ACCESS(res)}),
		new Test(public2, password, 'getting commands with public privileges from another company. Should fail with FORBIDDEN',
			function(res){return check.DEVICE_NO_ACCESS(res)}),
		new Test(public3, password, 'getting commands with public privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return check.DEVICE_NO_ACCESS(res)}),
		new Test(provisioner1, password, 'getting commands with provisioner privileges. Should succeed',
			function(res){return checkOkWrapper(res)}),
		new Test(provisioner2, password, 'getting commands with provisioner privileges from another company. Should fail with FORBIDDEN',
			function(res){return check.DEVICE_NO_ACCESS(res)}),
		new Test(provisioner3, password, 'getting commands with provisioner privileges from a parent company. Should succeed',
			function(res){return checkOkWrapper(res)}),
		new Test(designer1, password, 'getting commands with designer privileges. Should succeed',
			function(res){return checkOkWrapper(res)}),
		new Test(designer2, password, 'getting commands with designer privileges from another company. Should fail with FORBIDDEN',
			function(res){return check.DEVICE_NO_ACCESS(res)}),
		new Test(designer3, password, 'getting commands with designer privileges from a parent company. Should succeed',
			function(res){return checkOkWrapper(res)}),
		new Test(cmpAdmin1, password, 'getting commands with company Admin privileges. Should succeed',
			function(res){return checkOkWrapper(res)}),
		new Test(cmpAdmin2, password, 'getting commands with company Admin privileges from another company. Should fail with FORBIDDEN',
			function(res){return check.DEVICE_NO_ACCESS(res)}),
		new Test(cmpAdmin3, password, 'getting commands with company Admin privileges from a parent company. Should succeed',
			function(res){return checkOkWrapper(res)}),
	]
	check.permissions(function(){return authenticator.get(getQueueURL(macID))}, testArray)
})

describe('get with various bad parameters', function()
{
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function(res){return authenticator.put(loadToQueueURL, loadBody)})
		.then(function(res){return check.OK_DNE(res)})

	})
	after(function()
	{
		return clearQueue(macID)
		.finally(function(){return authenticator.pLogout(designer1)})
	})
	it('get queue from a nonExistant macID. Should fail with FORBIDDEN', function()
	{
		return authenticator.get(getQueueURL('this is a bad macID'))
		.then(function(res){return check.DEVICE_NO_ACCESS(res)})
	})
})

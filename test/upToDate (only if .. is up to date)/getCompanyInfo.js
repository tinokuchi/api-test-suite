//public users can call icGetCompanyGroups and icGetCompanies() when the shouldn't
//parents should retrieve company information on the children with the above two calls.
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const company1Id = 111
const company2Id = 112
const company3Id = 113

const company1EntryId = 48
const company3EntryId = 50

const company1CreateDate = "Thu Aug 23 19:32:09 2018"
const company3CreateDate = "Thu Aug 23 19:34:00 2018"

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//variables for testing
const getCompanyInfoURL = '/companies'

const checkBody1 =
{
	"companyList":
	{
		[company1]:
		{
			"companyId":company1Id,
			"createDate":company1CreateDate,
			"entryId":company1EntryId,
			"logo":[],
			"propertyMap": {"customPrefs":"", "mcRoleAccess":"true"},
			"enabled":true,
		}
	},
	"companyGroups":{[company1]:["Company Administrator", "Provisioner", "Designer", ]}
}
const checkBody3 =
{
	"companyList":
	{
		[company3]:
		{
			"companyId":company3Id,
			"createDate":company3CreateDate,
			"entryId":company3EntryId,
			"logo":[],
			"propertyMap":
			{
				"mcRoleIdList":company1Id.toString(),
				"customPrefs":"",
				"publicAccess":"true",
				"mcRoleAccess":"true",
			},
			"enabled":true,
		},
		[company1]:
		{
			"companyId":company1Id,
			"createDate":company1CreateDate,
			"entryId":company1EntryId,
			"logo":[],
			"propertyMap": {"customPrefs":"", "mcRoleAccess":"true"},
			"enabled":true,
		}
	},
	"companyGroups":
	{
		[company3]:["Company Administrator", "Provisioner", "Designer", ],
		[company1]:["Company Administrator", "Provisioner", "Designer", ],
	}
}
describe('get company Info with various privileges', function()
{
	var testArray =
	[
		new Test(null, null, 'getting company info without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting company info with public privileges. Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res)}),
		new Test(public3, password, 'getting company info with public privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res)}),
		new Test(provisioner1, password, 'getting company info with provisioner privileges. Should return info on the current company',
			function(res){return check.OK_Eqls(res, checkBody1)}),
		new Test(provisioner3, password, 'getting company info with provisioner privileges from a parent company. Should return info on both the current and child company',
			function(res){return check.OK_Eqls(res, checkBody3)}),
		new Test(designer1, password, 'getting company info with designer privileges. Should return info on the current company',
			function(res){return check.OK_Eqls(res, checkBody1)}),
		new Test(designer3, password, 'getting company info with designer privileges from a parent company. Should return info on both the current and child company',
			function(res){return check.OK_Eqls(res, checkBody3)}),
		new Test(cmpAdmin1, password, 'getting company info with company Admin privileges. Should return info on the current company',
			function(res){return check.OK_Eqls(res, checkBody1)}),
		new Test(cmpAdmin3, password, 'getting company info with company Admin privileges from a parent company. Should return info on both the current and child company',
			function(res){return check.OK_Eqls(res, checkBody3)}),
	]
	return check.permissions(function(){return authenticator.get(getCompanyInfoURL, null, null)}, testArray)
})

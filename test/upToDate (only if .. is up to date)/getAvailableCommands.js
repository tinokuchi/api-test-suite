//I don't verify what commands are available:
//I only verify that the response has a data payload.
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//constants for testing this function.
const getAvailableCommandsURL = '/commands'

describe('get available commands with various privileges', function()
{
	var testArray =
	[
		new Test(null, null, 'getting available commands without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting available commands with public privileges. Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res, true)}),
		new Test(provisioner1, password, 'getting available commands with provisioner privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
		new Test(designer1, password, 'getting available commands with designer privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
		new Test(cmpAdmin1, password, 'getting available commands with company Admin privileges. Should succeed',
			function(res){return check.OK_Contains(res, {})}),
	]
	check.permissions(function(){return authenticator.get(getAvailableCommandsURL)}, testArray)
})

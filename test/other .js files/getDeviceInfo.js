var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//variables for testing this request
var getDeviceInfoURL = function(devID){return "/devices/" + devID}

const macID1 = '00_1B_AD_00_EC_0C_35_40'
const macID2 = '00_1B_AD_00_EC_0C_35_00'
const macIDs = [macID1, macID2]

checkBody = function(macID){return {"name": "Device " + macID}}

describe('getting device info', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('should return information for the specified device', function()
	{
		return authenticator.get(getDeviceInfoURL(macID1))
		.then(function(res){return check.OK_Contains(res, checkBody(macID1))})
	})
})

describe("get device info with various privileges", function()
{
	testArray =
	[
		new Test(null, null, 'getting info without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
	]
	check.permissions(function(){return authenticator.get(getDeviceInfoURL(macID1))}, testArray)
})

describe("get with various bad parameters", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("getting a non-existant macID. Should fail with CONFLICT", function()
	{
		return authenticator.get(getDeviceInfoURL("this is a bad mac id"))
		.then(function(res){return check.DEVICE_NOT_FOUND(res)})
	})
})

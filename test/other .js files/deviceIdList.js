var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//Constants for testing this request
var deviceIdListURL = function(typeString){return "/devices/deviceIdList/" + typeString}
var type1 = "DEVICE_MLOCK"
var type2 = "DEVICE_IGATE"
var type3 = "DEVICE_ITAG"
var type1MacIDs = ["00_1B_AD_00_EC_0C_35_00","00_1B_AD_00_EC_0C_35_40"]


describe("get IDs of devices of a certain type", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("should return only the devices of the indicated type", function()
	{
		return authenticator.get(deviceIdListURL(type1))
		.then(function(res){return check.OK_Eqls(res, type1MacIDs)})
	})
})

describe("get IDs of devices of a type for which the user has none", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("should succeed but return an empty array", function()
	{
		return authenticator.get(deviceIdListURL(type3))
		.then(function(res){return check.OK_Eqls(res, [])})
	})
})

describe("get IDs of devices of a certain type with various different privileges", function()
{
	testArray =
	[
		new Test(null, null, 'getting IDs of devices by type without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
	]
	check.permissions(function(){return authenticator.get(deviceIdListURL(type1))}, testArray)
})

describe("get IDs of devices of a certain type with various bad parameters", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("get devices with type outside of enumeration bounds", function()
	{
		typeString = "DEVICE_TYPE_MAX_NUM"
		return authenticator.get(deviceIdListURL(typeString))
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("get devices with non-existant type", function()
	{
		typeString = "this type does not exist"
		return authenticator.get(deviceIdListURL(typeString))
		.then(function(res){return check.BAD_REQUEST(res)})
	})
})

var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//variables for testing this request
var getDeviceInfoListURL = function(deviceListString){return "/devices/infoList/" + deviceListString}

const type1MacIDs = ["00_1B_AD_00_EC_0C_35_40","00_1B_AD_00_EC_0C_35_00"]

const type1MacIDsString = JSON.stringify(type1MacIDs)

describe("test basic functionality", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("get only the device info for the requested devices", function()
	{
		return authenticator.get(getDeviceInfoListURL(type1MacIDsString))
		.then(function(res){return check.OK_HasKeys(res, type1MacIDs)})
	})
})

describe("get info for specified devices with various privileges", function()
{
	testArray =
	[
		new Test(null, null, 'getting info without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
	]
	check.permissions(function(){return authenticator.get(getDeviceInfoListURL(type1MacIDsString))}, testArray)
})

describe("get info for specific devices with various bad parameters", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("get with non-stringified array argument. Should return BAD_REQUEST", function()
	{
		return authenticator.get(getDeviceInfoListURL(type1MacIDs))
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("get a non-existant mac ID. Should succeed but return nothing", function()
	{
		return authenticator.get(getDeviceInfoListURL('["this is a bad macID"]'))
		.then(function(res){return check.OK_Eqls(res, {})})
	})
})

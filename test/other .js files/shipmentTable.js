//'icGetListSql()' does not filter by user's company: provided the client knows another company's companyId, they
//can find the other company's tables.
//Need to change the java so that it filters by company.
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'


//functions and variables to test this request
const shipmentTableURL = "/tables/shipmentTable"
const createDate = "2018-03-18 18:25:12"
const companyId1 = 111
const companyId2 = 112
const nonExistantCompanyId = 999

const getQuery1 =
{
	"date": createDate,
	"id": companyId1
}
const getQuery2 =
{
	"date": createDate,
	"id": companyId2
}
const checkBody =
{
	"0":
	{
		"entryId": "2196",
		"gateway": "none",
		"origin":"FortCampbell2Sept",
		"shippingState": "1",
		"count(*)":"1"
	}
}

describe("Get shipment table from company with a single shipment", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("Should return one shipment", function()
	{
		return authenticator.get(shipmentTableURL, null, getQuery1)
		.then(function(res){return check.OK_Eqls(res, checkBody)})
	})
})

describe("Get shipment table from company with no shipments", function()
{
	before(function(){return authenticator.pLogin(designer2, password)})
	after(function(){return authenticator.pLogout(designer2)})
	it("Should return no shipments", function()
	{
		return authenticator.get(shipmentTableURL, null, getQuery2)
		.then(function(res){return check.OK_NO_ENTRIES_RETURNED(res)})
	})
})

describe("Get shipment table with various privileges", function()
{
	var testArray1 =
	[
		new Test(null, null, 'getting shipment table without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting shipment table with public privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(public2, password, 'getting shipment table with public privileges from another company. Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res)}),
		new Test(public3, password, 'getting shipment table with public privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(provisioner1, password, 'getting shipment table with provisioner privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(provisioner2, password, 'getting shipment table with provisioner privileges from another company. Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res)}),
		new Test(provisioner3, password, 'getting shipment table with provisioner privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(designer1, password, 'getting shipment table with designer privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(designer2, password, 'getting shipment table with designer privileges from another company.  Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res)}),
		new Test(designer3, password, 'getting shipment table with designer privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(cmpAdmin1, password, 'getting shipment table with company Admin privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
		new Test(cmpAdmin2, password, 'getting shipment table with company Admin privileges from another company. Should  Should fail with FORBIDDEN',
			function(res){return check.NO_PRIVILEGES(res)}),
		new Test(cmpAdmin3, password, 'getting shipment table with company Admin privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkBody)}),
	]
	check.permissions(function(){return authenticator.get(shipmentTableURL, null, getQuery1)}, testArray1)
})

describe("Get shipment table with various bad parameters", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("Getting shipment table for non-existant company. Should fail with FORBIDDEN", function()
	{
		const getQuery =
		{
			"date": createDate,
			"id": nonExistantCompanyId
		}
		return authenticator.get(shipmentTableURL, null, getQuery)
		.then(function(res){return check.OK_NO_ENTRIES_RETURNED(res)})
	})
})

var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');

//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//Templates for manipulating maps
//"getBodies" are used for both get and delete
const getSetURL = '/geozone/getSet'
const createSetURL = '/geozone/createSet'
const deleteSetURL = '/geozone/deleteSet'
const updateSetURL = '/geozone/updateSet'

const blankMapName = "BlankMapName";
const fullMapName = "FullMapName";
const uniqueMapName = "UniqueMapName";

const getBlankQuery =
{
	"mapName":blankMapName,
	"companyName":company1,
}
const getFullQuery =
{
	"mapName":fullMapName,
	"companyName":company1,
}
const getUniqueQuery =
{
	"mapName":uniqueMapName,
	"companyName":company1,
}
const deleteBlankBody =
{
	"mapName":blankMapName,
	"companyName":company1,
}
const deleteFullBody =
{
	"mapName":fullMapName,
	"companyName":company1,
}
const deleteUniqueBody =
{
	"mapName":uniqueMapName,
	"companyName":company1,
}
const createBlankBody =
{
	"mapName": blankMapName,
	"companyName": company1,
	"map":{},
}
const createFullBody =
{
	"mapName": fullMapName,
	"companyName": company1,
	"map":
	{
		"points":
		[
       			{ latitude: 37.360573, longitude: -121.940918, pointType:"point" },
       			{ latitude: 37.410782, longitude: -122.025856, pointType:"point" },
       			{ latitude: 37.798122, longitude: -122.397926, pointType:"point" },
		],
		"description": "this is a description",
		"subMapID": [],
		"altNames": [ fullMapName + "1", fullMapName + "2"],
		"enabled": false,
	},
}
const checkBlankBody =
{
	'mapName': blankMapName,
	'companyName':company1,
	'points':[],
	"description":"",
	"subMapID":[],
	"altNames":[],
	"enabled":true,
}
const checkFullBody =
{
	'mapName': fullMapName,
	'companyName':company1,
	'points':
	[
				{ latitude: 37.360573, longitude: -121.940918},
				{ latitude: 37.410782, longitude: -122.025856},
				{ latitude: 37.798122, longitude: -122.397926},
	],
	"description": "this is a description",
	"subMapID": [],
	"altNames": [ fullMapName + "1", fullMapName + "2"],
	"enabled": false,
}


//Tests
/*
describe('delete orphaned maps', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('cleaning. Should always succeed.', function()
	{
		return authenticator.delete(deleteSetURL,deleteBlankBody)
		.then(function(){return authenticator.delete(deleteSetURL,deleteFullBody)})
		.then(function(){return authenticator.delete(deleteSetURL,deleteUniqueBody)})
	})
})
*/

describe('update without any optional parameters', function()
{
	const updateBody =
	{
		"mapName": fullMapName,
		"companyName": company1,
		"map":{},
	}
	it('should not alter the map set', function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createSetURL, createFullBody)
			.then(function(res){return check.CREATED_DNE(res)})
			.then(function()
			{
				return authenticator.put(updateSetURL, updateBody)
				.then(function(res){return check.OK_DNE(res)})
				.then(function()
				{
					return authenticator.get(getSetURL, null, getFullQuery)
					.then(function(res){return check.OK_Eqls(res, checkFullBody)})
				})
				.finally(function(){return authenticator.delete(deleteSetURL, deleteFullBody)})
			})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
})

describe('update with all optional parameters except subMapID, enabled, newMapName, and newCompanyName empty', function()
{
	const updateBody =
	{
		"mapName": fullMapName,
		"companyName": company1,
		"map":
		{
			"points":[],
			"description":"",
			"altNames":[],
		},
	}
	const checkBody =
	{
		'mapName': fullMapName,
		'companyName':company1,
		'points':[],
		"description": "",
		"subMapID": [],
		"altNames": [],
		"enabled": false,
	}
	it('should delete all fields of the mapSet', function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createSetURL, createFullBody)
			.then(function(res){return check.CREATED_DNE(res)})
			.then(function()
			{
				return authenticator.put(updateSetURL, updateBody)
				.then(function(res){return check.OK_DNE(res)})
				.then(function()
				{
					return authenticator.get(getSetURL, null, getFullQuery)
					.then(function(res){return check.OK_Eqls(res, checkBody)})
				})
				.finally(function(){return authenticator.delete(deleteSetURL, deleteFullBody)})
			})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
})


//creates a blank map, updates it to a full map, then deletes the full map.
//If the delete fails because there is no full map, then that means the update failed,
//and we delete the blank map.
describe('update with all optional parameters except subMapID and newCompanyName filled', function()
{
	const updateBody =
	{
		"mapName": blankMapName,
		"companyName": company1,
		"map":
		{
			"newMapName":fullMapName,
			"points":
			[
               			{ latitude: 37.360573, longitude: -121.940918},
               			{ latitude: 37.410782, longitude: -122.025856},
               			{ latitude: 37.798122, longitude: -122.397926},
			],
			"description": "this is a description",
			"subMapID": [],
			"altNames": [ fullMapName + "1", fullMapName + "2"],
			"enabled": false,
		},
	}
	it('should update all fields of the map set', function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createSetURL, createBlankBody)
			.then(function(res){return check.CREATED_DNE(res)})
			.then(function()
			{
				return authenticator.put(updateSetURL, updateBody)
				.then(function(res){return check.CREATED_DNE(res)})
				.then(function()
				{
					return authenticator.get(getSetURL, null, getFullQuery)
					.then(function(res){return check.OK_Eqls(res, checkFullBody)})
					.finally(function(){return authenticator.delete(deleteSetURL, deleteFullBody)})
				},function(error)
				{
					return authenticator.delete(deleteSetURL, deleteBlankBody)
					.then(function(){return Promise.reject(error)})
				})
			})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
})

/*
	Actually goes through and verifies that the update went through,
	which is extraneous at this stage. If refactored, we could
	probably just check the return code and call it a day.
*/
describe('update a map set with various privileges', function()
{
	const updateBody =
	{
		"mapName": blankMapName,
		"companyName":company1,
		"map":{"description":"this is a description"},
	}
	const checkBody =
	{
		'mapName': blankMapName,
		'companyName':company1,
		'points':[],
		"description": "this is a description",
		"subMapID": [],
		"altNames": [],
		"enabled": true,
	}
	//Since, for some users, we expect to request an update and
	//have that request rejected, we must delete and recreate the map
	//in the error case where the request is erroneously accepted.
	var noPrivilegesWrapper = function(res, doPrint=false)
	{
		return check.NO_PRIVILEGES(res, doPrint)
		.catch(function(error)
		{
			return new Promise(function(resolve, reject)
			{
				authenticator.delete(deleteSetURL, deleteBlankBody)
				.then(function(res){return check.OK_DNE(res, doPrint)})
				.then(function(){return authenticator.post(createSetURL, createBlankBody)})
				.then(function(res){return check.CREATED_DNE(res, doPrint)})
				.finally(function(){reject(error)})
			})
		})
	}
	//For all users that we expect to have update privileges,
	//we must delete and recreate the map once we verify that the create
	//has succeeded.
	var okDneWrapper = function(res, doPrint)
	{
		return check.OK_DNE(res, doPrint)
		.then(function(){return authenticator.get(getSetURL, null, getBlankQuery)})
		.then(function(res){return check.OK_Eqls(res, checkBody)})
		.then(function()
		{
			return authenticator.delete(deleteSetURL, deleteBlankBody)
			.then(function(res){return check.OK_DNE(res, doPrint)})
			.then(function(){return authenticator.post(createSetURL, createBlankBody)})
			.then(function(res){return check.CREATED_DNE(res, doPrint)})
		})
	}
	before(function(done)
	{
		authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createSetURL, createBlankBody)
			.then(function(res){return check.CREATED_DNE(res)})
			.finally(function(){authenticator.cLogout(designer1, done)})
		})
	})
	after(function(done)
	{
		authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.delete(deleteSetURL, deleteBlankBody)
			.finally(function(){authenticator.cLogout(designer1, done)})
		})
	})
	var testArray =
	[
		new Test(null, null, 'updating a map set without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'updating a map set with public privileges. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(public2, password, 'updating a map set with public privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(public3, password, 'updating a map set with public privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(provisioner1, password, 'updating a map set with provisioner privileges. Should  fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(provisioner2, password, 'updating a map set with provisioner privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(provisioner3, password, 'updating a map set with provisioner privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(designer1, password, 'updating a map set with designer privileges. Should succeed',
			function(res){return okDneWrapper(res)}),
		new Test(designer2, password, 'updating a map set with designer privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(designer3, password, 'updating a map set with designer privileges from a parent company. Should succeed',
			function(res){return okDneWrapper(res)}),
		new Test(cmpAdmin1, password, 'updating a map set with company Admin privileges. Should succeed',
			function(res){return okDneWrapper(res)}),
		new Test(cmpAdmin2, password, 'updating a map set with company Admin privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(cmpAdmin3, password, 'updating a map set with company Admin privileges from a parent company. Should succeed',
			function(res){return okDneWrapper(res)}),
	]
	return check.permissions(function(){return authenticator.put(updateSetURL, updateBody)}, testArray)
})


describe('Update a mapset with various bad parameters', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})

	it('update a map set to have a duplicate name. Should fail with CONFLICT', function()
	{
		const createUniqueBody =
		{
			"mapName":uniqueMapName,
			"companyName":company1,
			"map":{}
		}
		const updateBody =
		{
			"mapName":blankMapName,
			"companyName":company1,
			"map":{"newMapName":uniqueMapName}
		}
		return authenticator.post(createSetURL, createBlankBody)
		.then(function(res){return check.CREATED_DNE(res)})
		.then(function()
		{
			return authenticator.post(createSetURL, createUniqueBody)
			.then(function(res){return check.CREATED_DNE(res)})
			.then(function()
			{
				return authenticator.put(updateSetURL, updateBody)
				.then(function(res){return check.DUPLICATE_NAME(res)})
				.then(function(){return authenticator.delete(deleteSetURL, deleteUniqueBody)})
				.catch(function(error)
				{
					return authenticator.delete(deleteSetURL, deleteBlankBody)
					.then(function(){return Promise.reject(error)})
				})
			})
			.finally(function(){return authenticator.delete(deleteSetURL, deleteBlankBody)})
		})
	})
	it('update a map set to have a duplicate alternate name. Should fail with CONFLICT', function()
	{
		const createUniqueBody =
		{
			"mapName":uniqueMapName,
			"companyName":company1,
			"map":{"altNames":['alternateMapName1']}
		}
		const updateBody =
		{
			"mapName":blankMapName,
			"companyName":company1,
			"map":{"altNames":['alternateMapName1']}
		}
		return authenticator.post(createSetURL, createBlankBody)
		.then(function(res){return check.CREATED_DNE(res)})
		.then(function()
		{
			return authenticator.post(createSetURL, createUniqueBody)
			.then(function(res){return check.CREATED_DNE(res)})
			.then(function()
			{
				return authenticator.put(updateSetURL, updateBody)
				.then(function(res){return check.DUPLICATE_NAME(res)})
				.then(function(){return authenticator.delete(deleteSetURL, deleteUniqueBody)})
				.catch(function(error)
				{
					return authenticator.delete(deleteSetURL, deleteBlankBody)
					.then(function(){return Promise.reject(error)})
				})
			})
			.finally(function(){return authenticator.delete(deleteSetURL, deleteBlankBody)})
		})
	})
	it('update a map set with a missing parameter. Should fail with BAD_REQUEST', function()
	{
		const updateBody =
		{
			"mapName":blankMapName,
			"companyName":company1
		}
		return authenticator.post(createSetURL, createBlankBody)
		.then(function(res){return check.CREATED_DNE(res)})
		.then(function()
		{
			return authenticator.put(updateSetURL, updateBody)
			.then(function(res){return check.BAD_REQUEST(res)})
			.then(function(){return authenticator.delete(deleteSetURL, deleteUniqueBody)})
			.catch(function(error)
			{
				return authenticator.delete(deleteSetURL, deleteBlankBody)
				.then(function(){return Promise.reject(error)})
			})
		})
		.finally(function(){return authenticator.delete(deleteSetURL, deleteBlankBody)})
	})
})

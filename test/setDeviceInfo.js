/*
	This file is a good example of everything except a full permissions check.
*/
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');

//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//constants, variables, and functions specific to this request
/*
	If a URL contains a path parameter, I declare it as a
	function that takes as an argument that path parameter
	and returns the URL string.
*/
var getDeviceInfoURL = function(devID){return "/devices/" + devID}
const setDeviceInfoURL = "/devices/setInfo"

const macID1 = '00_1B_AD_00_EC_0C_35_40'
const macIDBad = "bad mac id"
const originalName = 'Device 00_1B_AD_00_EC_0C_35_40'
const originalStatus = 'PROVISION_NONE'
const originalNotes = ""
const newName = "Device newName"
const newStatus = "PROVISION_RESERVED"
const newNotes = "this device is for testing"

const setStatusOriginal = "none"
const setStatusNew = "reserved"
const setStatusBad = "bad"

/*
	"checkBodies" are used with check.OK_Eqls.
	They are exact copies of what I expect
	back from the "data" portion of the payload.

	This is a general naming scheme I follow
	throughout the framework.
*/
const checkBodyTemplate =
{
	entryId: '13',
	inventoryList: '[]',
	inventoryParent: '00_1B_AD_00_EC_0C_35_40',
	inventoryServer: '',
	inventorySource: '',
	inventoryStatus: '',
	location: '',
	permissions:
	'{Sandia=[Provisioner], iControl Inc.=[Provisioner, Designer], testCompany1=[Company Administrator, Provisioner, Designer]}',
	messages:
	{
		'Status (60/0)': { pkttype: '60', msgtype: '0', messages: '' },
		'Config CRC (0/10)': { pkttype: '0', msgtype: '10', messages: '' },
		'Cell Status (0/11)': { pkttype: '0', msgtype: '11', messages: '' }
	}
}
const checkOriginalBody =
{
	name: originalName,
	status: originalStatus,
	notes: originalNotes,
	...checkBodyTemplate
}
const checkNewBody =
{
	name: newName,
	status: newStatus,
	notes: newNotes,
	...checkBodyTemplate
}
const checkEmptyBody =
{
	name:"",
	status: originalStatus,
	notes:"",
	...checkBodyTemplate
}
/*
	"updateBodies" are what get passed to update functions, such as
	updateMapSet or setDeviceInfo.
*/
const updateOriginalBody = {"dev_id":macID1, "name":originalName, "notes":originalNotes, "status":setStatusOriginal}
const updateNewBody = {"dev_id":macID1, "name":newName, "notes":newNotes, "status":setStatusNew}
const updateBlankBody = {"dev_id":macID1}
const updateEmptyBody = {"dev_id":macID1, "name":"", "notes":""}

/*
	I found myself executing these exact calls to clean up after myself
	multiple times, so they get their own function

	Specific to this file.
*/
function revertUpdate(macID)
{
	return authenticator.put(setDeviceInfoURL, updateOriginalBody)
	.then(function(res){return check.OK_DNE(res)})
	.then(function(){return authenticator.get(getDeviceInfoURL(macID))})
	.then(function(res){return check.OK_Eqls(res, checkOriginalBody)})
}

/*
	The first actual test. Mocha works by synchronously executing
	every "describe" block it sees. Within each "describe" block,
	each "it" block is also executed in order.

	The first set of tests are testing the basic functionality:
	whether or not the function does what its supposed to when
	called correctly.
*/
/*

	The strings passed to "describe" and "it" get printed to the
	console and are purely for readability. I tend to use the
	describe string for what the test is doing, and the it string
	for what I expect to get back.
*/
describe('update device info with all optional parameters blank', function()
{
	/*
		Aside from "describe" and "it", mocha also offers four "hooks":
		"before", "after", "beforeEach", and "afterEach".

		"before" and "after" are run before  or after any "it" or "describe"
		blocks in the current scope, while "beforeEach" and "afterEach" are
		ran after every "it" or "describe" blocks in the current scope.

		"after" and "afterEach" hooks are executed regardless of whether
		the assertions in the "it" block succeed.

		Mocha will identify where an assertion error originates. Thus,
		I try to set-up and breakdown tests in hooks, while
		using the "it" blocks to execute the test itself. That way,
		if the set-up fails, the error occurs from a hook instead
		of the test.

		The "it" blocks as well as all hooks use promises by default.
		Specifically, they fail if they are returned a rejected promise,
		and succeed if they are returned a resolved promise.
		Behavior when nothing is returned, or a non-promise is returned
		varies from timeout failures to instantly succeeding.
	*/
	before(function(){return authenticator.pLogin(cmpAdmin1, password)})
	after(function(){return authenticator.pLogout(cmpAdmin1)})
	it('Should not alter the device info', function()
	{
		/*
			The functions I wrote come in two major categories.

			"authenticator" functions are used to make a request.
			The function name specifies the type of request (ex: authenticator.get() vs authenticator.delete()),
			the first argument is the URL to send the request to,
			the second argument is the payload to send (defaults to "null")
			the third argument is the queries to send (defaults to "null")
			and the fourth argument is whether or not to print the response to the console (defaults to "false")
			Authenticator functions always succeed and always resolve a promise,
			with the response as the resolved object.

			"check" functions are used to test the response, and must be promise chained from an
			"authenticator" function to work properly.
			The first argument is the response object resolved by an authenticator function.
			The second argument varies.
				If the "check" function expects the response to contain "data",
				then the second argument is: an exact copy of that data object in the case of "check.XX_Eqls()";
				a subset of that data object in the case of "check.XX_Contains()";
				an exhaustive array of keys that the data object contains in the case of "check.XX_HasKeys()";
			The last argument is whether or not to print the response to the console (defaults to "false").
				This last argument is identical to the last argument in an authenticator function.

			In this particular promise chain, I send a "put" request to modify a device, then check if it returned 200 OK
			with a data payload that Does Not Exist. If that succeeds, I send a "get" request for the device I just modified,
			then check if the get request returned 200 OK with a data payload that Equals "checkOriginalBody".
		*/
		return authenticator.put(setDeviceInfoURL, updateBlankBody)
		.then(function(res){return check.OK_DNE(res)})
		.then(function(){return authenticator.get(getDeviceInfoURL(macID1))})
		.then(function(res){return check.OK_Eqls(res, checkOriginalBody )})
	})
})

describe('update device info with all optional parameters filled', function()
{
	before(function(){return authenticator.pLogin(cmpAdmin1, password)})
	after(function()
	{
		return revertUpdate(macID1)
		.finally(function(){return authenticator.pLogout(cmpAdmin1)})
	})
	it('Should alter all indicated device fields', function()
	{
		return authenticator.put(setDeviceInfoURL, updateNewBody)
		.then(function(res){return check.OK_DNE(res)})
		.then(function(){return authenticator.get(getDeviceInfoURL(macID1))})
		.then(function(res){return check.OK_Eqls(res, checkNewBody)})
	})
})

describe('update device info with "notes" and "name" fields present, but blank', function()
{
	before(function(){return authenticator.pLogin(cmpAdmin1, password)})
	after(function()
	{
		return revertUpdate(macID1)
		.finally(function(){return authenticator.pLogout(cmpAdmin1)})
	})
	it('Should alter all indicated device fields', function()
	{
		return authenticator.put(setDeviceInfoURL, updateEmptyBody)
		.then(function(res){return check.OK_DNE(res)})
		.then(function(){return authenticator.get(getDeviceInfoURL(macID1))})
		.then(function(res){return check.OK_Eqls(res, checkEmptyBody)})
	})
})

/*
	The second set of tests are privilege tests: whether or not
	the correct users are allowed to make the request or not.
*/
describe('update device info with various privileges', function()
{
	/*
		These wrapper functions combine permissions checking with
		test clean up into one function, which can be passed in
		as the fourth argument to a "Test" constructor. See below for
		more on "Test" constructors and their use in "check.permissions()"

		All update, create, or delete functions require clean up in the case
		of successful completion, so that the testing environment is kept
		consistent for further tests. Thus, we have these wrappers.

		In general, privilege checking requires at least two wrappers:
		one "access" case, and one "no access case".
			- The access case wrapper should be called when a user needs to
			have access to the request. It verifies that the user
			successfully completed the request, and if successful,
			reverts the effects of that request.
			- The no access case wrapper should be called when a user
			should no have access to the request. It verifies that
			the user was prevented from completing the request,
			but if the request was erroneously accepted,
			reverts the effects of that request.
			- In this specific file, we have three wrappers. Because both
			"noAccessWrapper" and "okWrapper" need to revert a request,
			I made a "revertWrapper" to avoid writing code twice.
	*/
	function revertWrapper(macID)
	{
		return authenticator.pLogin(cmpAdmin1, password)
		.then(function()
		{
			return revertUpdate(macID)
			.finally(function(){return authenticator.pLogout(cmpAdmin1)})
		})
	}
	function noAccessWrapper(res, doPrint = false)
	{
		return check.DEVICE_NO_ACCESS(res, doPrint)
		.catch(function(error)
		{
			return revertWrapper(macID1)
			.then(function(){return Promise.reject(error)})
		})
	}
	function okWrapper(res, doPrint = false)
	{
		return check.OK_DNE(res, doPrint)
		.then(function(){return revertWrapper(macID1)})
	}
	/*
		"check.permissions()" runs a single request across multiple different users.
		Internally, it calls the "it()" function inside, which is why no it() blocks
		appear here.

		check.permissions() takes as its first argument the request to be run.
			Internally, check.permissions() does not pass any arguments to this request, so it must be
			wrapped in an anonymous function.
		check.permissions() takes as its second argument an array of "Test" objects
		Each test object contains all the information necessary to create and run a test on a single user.
		The constructor for a single Test object takes four arguments:
			The first and second are the username and password of the user to be tested. If "null" is
			passed for both, then the request is tested without logging in.
			The third argument is the "it" string to be printed to the console.
			The fourth argument is a "check" function to be promise chained into.
				Note the wrapping anonymous function must take a single argument, which is then passed to the
				underlying "check" function as its first argument.
				In many cases, clean up is necessary after a test, depending on whether the test succeeded or not.
				In these cases, it is necessry to add clean up to the fourth argument, which is where the
				wrapper function comes in. See above.
	*/
	testArray =
	[
		/*
			I never write a "check.UNAUTHORIZED" wrapper because I'm reasonably sure
			it will return successfully, based on how the java is written.
		*/
		new Test(null, null, 'setting device info without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
	]
	check.permissions(function(){return authenticator.put(setDeviceInfoURL, updateNewBody)}, testArray)
})


/*
	The last set of tests are for the failure cases.

	The "check" functions for failure cases are titled according to the
	ic api return codes, while the "it" strings are titled according to the http code.
	This is because Mike felt that a user shouldn't need to care about the ic api codes
	while running the tests, but that the test itself should still verify the ic api code.

	A few of the "check" functions are labeled according to http codes, such as "BAD_REQUEST"
	and "INTERNAL_SERVER_ERROR". These are for when an error is caught by the backend rather than
	the ic api, and thus there is no associated ic api failure code.
*/
describe('update device info with various bad parameters', function()
{
	before(function(){return authenticator.pLogin(cmpAdmin1, password)})
	after(function(){return authenticator.pLogout(cmpAdmin1)})
	it('update device info without a required argument. Should fail with BAD_REQUEST', function()
	{
		updateBody = {"name":originalName, "notes":originalNotes, "status":setStatusOriginal}
		return authenticator.put(setDeviceInfoURL, updateBody)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it('update device info with a bad status string. Should fail with BAD_REQUEST', function()
	{
		updateBody = {"dev_id":macID1, "name":originalName, "notes":originalNotes, "status":setStatusBad}
		return authenticator.put(setDeviceInfoURL, updateBody)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it('update device info with a bad device ID. Should fail with CONFLICT', function()
	{
		updateBody = {"dev_id":macIDBad, "name":originalName, "notes":originalNotes, "status":setStatusBad}
		return authenticator.put(setDeviceInfoURL, updateBody)
		.then(function(res){return check.DEVICE_NOT_FOUND(res)})
	})
})

var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//Templates for manipulating maps
const tableName = 'shippingRouteMapAssociations';
const getEntryURL = '/tables/' + tableName + '/entries';
const createEntryURL = '/tables/create/' + tableName;
const deleteEntryURL = '/tables';

const nonExistantTableName = 'thisTableNameDoesNotExist';
const createFakeEntryURL = '/tables/create/' + nonExistantTableName;

const companyId1 = 111;
const description = 'this is a description';

const fullName = "FullName";

const getFullQuery =
{
	where: "name='" + fullName + "'",
	field:["name","companyId","description"],
}
const checkFullBody=
{
	'0':
	{
		name: fullName,
		companyId: companyId1.toString(),
		description: description,
	}
}
const createFullBody =
{
	'name':{'type':'string', 'value':fullName},
	'companyId':{'type':'integer', 'value':companyId1},
	'description':{'type':'string', 'value':description},
}
const deleteFullBody =
{
	'table':tableName,
	'where':'name="' + fullName + '"',
}

/*
describe('delete orphaned entries', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('cleaning orphans', function()
	{
		return authenticator.delete(deleteEntryURL, deleteFullBody)
	})
})
*/

describe('create an entry', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('Should succeed and have the appropriate contents', function()
	{
		return authenticator.post(createEntryURL, createFullBody)
		.then(function(res)
		{
			return check.CREATED_ENTRYID(res)
			.then(function(){return authenticator.get(getEntryURL, null, getFullQuery)})
			.then(function(res){return check.OK_Eqls(res, checkFullBody)})
			.finally(function(){return authenticator.delete(deleteEntryURL, deleteFullBody)})
		})
	})
})

describe('create an entry with various privileges', function()
{
	var requiresDelete = false
	//Since, for some users, we expect to request a create and
	//have that request rejected, we must delete the map
	//in the error case where the request is erroneously accepted.
	var noPrivilegesWrapper = function(res, doPrint=false)
	{
		return check.NO_PRIVILEGES(res, doPrint)
		.catch(function(error)
		{
			requiresDelete = true
			return Promise.reject(error)
		})
	}
	//For all users that we expect to have create privileges,
	//we must delete the map once we verify that the create
	//has succeeded.
	var createdDneWrapper = function(res, doPrint)
	{
		return check.CREATED_ENTRYID(res, doPrint)
		.then(function()
		{
			requiresDelete = true
			return Promise.resolve(res)
		})
	}
	afterEach(function()
	{
		if(requiresDelete)
		{
			return authenticator.pLogin(designer1, password)
			.then(function()
			{
				return authenticator.delete(deleteEntryURL, deleteFullBody)
				.finally(function()
				{
					requiresDelete = false
					return authenticator.pLogout(designer1)
				})
			})
		}
		else
			return Promise.resolve()
	})
	var testArray =
	[
		new Test(null, null, 'creating an entry without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'creating an entry with public privileges. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(public2, password, 'creating an entry with public privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(public3, password, 'creating an entry with public privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(provisioner1, password, 'creating an entry with provisioner privileges. Should  fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(provisioner2, password, 'creating an entry with provisioner privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(provisioner3, password, 'creating an entry with provisioner privileges from a parent company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(designer1, password, 'creating an entry with designer privileges. Should succeed',
			function(res){return createdDneWrapper(res)}),
		new Test(designer2, password, 'creating an entry with designer privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(designer3, password, 'creating an entry with designer privileges from a parent company. Should succeed',
			function(res){return createdDneWrapper(res)}),
		new Test(cmpAdmin1, password, 'creating an entry with company Admin privileges. Should succeed',
			function(res){return createdDneWrapper(res)}),
		new Test(cmpAdmin2, password, 'creating an entry with company Admin privileges from another company. Should fail with FORBIDDEN',
			function(res){return noPrivilegesWrapper(res)}),
		new Test(cmpAdmin3, password, 'creating an entry with company Admin privileges from a parent company. Should succeed',
			function(res){return createdDneWrapper(res)}),
	]
	return check.permissions(function(){return authenticator.post(createEntryURL, createFullBody)}, testArray)
})

describe('create an entry with various bad parameters', function()
{
	before(function(){return authenticator.pLogin(designer1,password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('create an entry with a duplicate name. Should fail with CONFLICT', function()
	{
		return authenticator.post(createEntryURL, createFullBody)
		.then(function(res){return check.CREATED_ENTRYID(res)})
		.then(function()
		{
			return authenticator.post(createEntryURL, createFullBody)
			.then(function(res){return check.DUPLICATE_NAME(res)})
			.catch(function(error)
			{
				return authenticator.delete(deleteEntryURL, deleteFullBody)
				.then(function(){return Promise.reject(error)})
			})
			.finally(function(){return authenticator.delete(deleteEntryURL, deleteFullBody)})
		})
	})
/*
	it('create an entry with a non-existant tableName. Should fail with CONFLICT', function()
	{
		return authenticator.post(createEntryURL, createFullBody)
		.then(function(res){return check.UNKNOWN_TABLE})
		.catch(function(error)
		{
			return authenticator.delete(deleteEntryURL, deleteFullBody)
			.then(function(){return Promise.reject(error)})
		})
	})
*/
})

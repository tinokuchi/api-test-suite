//Rewrite for shippingPickList
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//Templates for manipulating maps
const tableName = 'shippingRouteMapAssociations';
const getEntryURL = '/tables/' + tableName + '/entries';
const createEntryURL = '/tables/create/' + tableName;
const deleteEntryURL = '/tables';

const nonExistantTableName = 'thisTableNameDoesNotExist';
const getFakeEntryURL = '/tables/' + nonExistantTableName + '/entries';

const companyId1 = 111;
const description = 'this is a description';

const fullName = "FullName";

const getFullQuery =
{
	where: "name='" + fullName + "'",
	field:["name","companyId","description"],
}
const checkFullBody=
{
	'0':
	{
		name: fullName,
		companyId: companyId1.toString(),
		description: description,
	}
}
const createFullBody =
{
	'name':{'type':'string', 'value':fullName},
	'companyId':{'type':'integer', 'value':companyId1},
	'description':{'type':'string', 'value':description},
}
const deleteFullBody =
{
	'table':tableName,
	'where':'name="' + fullName + '"',
}

describe('delete orphaned entries', function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it('cleaning orphans', function()
	{
		return authenticator.delete(deleteEntryURL, deleteFullBody, null, true)
		.then(function(){return authenticator.delete(deleteEntryURL, deleteFullBody, null, true)})
	})
})

describe('check that data is retrieved properly', function()
{
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createEntryURL, createFullBody)
			.then(function(res){return check.CREATED_ENTRYID(res)})
		})
	})
	after(function()
	{
		return authenticator.delete(deleteEntryURL, deleteFullBody)
		.finally(function(){return authenticator.pLogout(designer1)})
	})
	it('retrieve a map and check the contents', function()
	{
		return authenticator.get(getEntryURL, null, getFullQuery)
		.then(function(res){return check.OK_Eqls(res, checkFullBody)})
	})
})

describe('get with various privileges', function()
{
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createEntryURL, createFullBody)
			.then(function(res){return check.CREATED_ENTRYID(res)})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	after(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.delete(deleteEntryURL, deleteFullBody)
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	var testArray =
	[
		new Test(null, null, 'getting a map set without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting a map set with public privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
		new Test(public2, password, 'getting a map set with public privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(public3, password, 'getting a map set with public privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
		new Test(provisioner1, password, 'getting a map set with provisioner privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
		new Test(provisioner2, password, 'getting a map set with provisioner privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(provisioner3, password, 'getting a map set with provisioner privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
		new Test(designer1, password, 'getting a map set with designer privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
		new Test(designer2, password, 'getting a map set with designer privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(designer3, password, 'getting a map set with designer privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
		new Test(cmpAdmin1, password, 'getting a map set with company Admin privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
		new Test(cmpAdmin2, password, 'getting a map set with company Admin privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(cmpAdmin3, password, 'getting a map set with company Admin privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, checkFullBody)}),
	]
	return check.permissions(function(){return authenticator.get(getEntryURL, null, getFullQuery)}, testArray)
})


describe('get from a nonexistant table', function()
{
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createEntryURL, createFullBody)
			.then(function(res){return check.CREATED_ENTRYID(res)})
		})
	})
	after(function()
	{
		return authenticator.delete(deleteEntryURL, deleteFullBody)
		.finally(function(){return authenticator.pLogout(designer1)})
	})
	it('Attempt to retrieve from a nonexistant table. Should fail.', function()
	{
		return authenticator.get(getFakeEntryURL, null, getFullQuery)
		.then(function(res){return check.UNKNOWN_TABLE(res)})
	})
})

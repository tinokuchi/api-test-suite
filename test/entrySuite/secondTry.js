//getting from shippingPickList unavailable to everyone except designers: 600_ERR_NO_PRIVILEGES
//parent company cannot see child company's shippingPickList entries.

//can't get from "groups" as any user
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const companyId1 = 111
const companyId2 = 112
const companyId3 = 113

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'


//global variables and functions for testing this request
const pLATable = 'cellLocations'			//public level access
const gLATable = 'shippingEventLog'			//guid level access
const cLATable = 'shippingPickList'			//company level access
const uLATable = 'users'					//user level access
const cALATable = 'groups'					//company admin level access
const aLATable = 'serverConfig'				//admin level access
const aOTable = 'control'					//admin only
const nonExistantTableName = 'thisTableNameDoesNotExist'

var getEntryURL = function(tableName){return '/tables/' + tableName + '/entries'}
var createEntryURL = function(tableName){return '/tables/create/' + tableName}
const deleteEntryURL = '/tables'

//I'll need getBody for aO
//I'll need get,create,update,delete bodies for cLA, uLA, cALA, and aLA
//public users: cLA get success, cLA put/post failure, aLA get failure
//provisioner users: cLA get success, cLA put/post failure, aLA get failure
//designer users: cLA get success, cLA put/post success, aLA get failure, cALA put/post failure
//company admin users: cLA get success, uLA put/post success, aO get failure, cLA get failure

const cLAName = "cLAName"
const cLAGetQuery =
{
	where: "name='" + cLAName + "'",
	field:["name","companyId"],
}
const cLACreateBody =
{
	'name':{'type':'string', 'value':cLAName},
}
const cLADeleteBody =
{
	'table':cLATable,
	'where':'name="' + cLAName + '"',
}
const cLACheckBody =
{
	'0':
	{
		"name": cLAName,
		"companyId": companyId1.toString(),
	}
}

const cALAName = "cALAName"
const cALAGetQuery ={}
/*
{
	where: "groupName='" + cALAName + "'",
	field:["groupName", "companyId"],
}
*/
const cALACreateBody =
{
	"groupName":{"type":"string", "value":cALAName},
}
const cALADeleteBody =
{
	"table":cALATable,
	"where":'groupName="' + cALAName + '"',
}
const cALACheckBody =
{
	'0':
	{
		"groupName": cALAName,
		"companyId":companyId1.toString(),
	}
}

const aLAName = "LinuxDev"
const aLAGetQuery =
{
	where: "name='" + aLAName + "'",
	sort: "entryId desc",
	max: 1,
	field:["name"],
}
const aLACheckBody =
{
	'0':{"name":aLAName}
}

/*
describe("delete orphaned company level access table entries", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("cleaning orphans", function()
	{
		return authenticator.delete(deleteEntryURL, cLADeleteBody)
	})
})

describe("get an entry from a company level access table with various privileges", function()
{
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createEntryURL(cLATable), cLACreateBody)
			.then(function(res){return check.CREATED_ENTRYID(res, true)})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	after(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.delete(deleteEntryURL, cLADeleteBody)
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	var testArray =
	[
		new Test(null, null, 'getting an entry without logging in. Should fail with UNAUTHORIZED',
			function(res){return check.UNAUTHORIZED(res)}),
		new Test(public1, password, 'getting an entry with public privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
		new Test(public2, password, 'getting an entry with public privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(public3, password, 'getting an entry with public privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
		new Test(provisioner1, password, 'getting an entry with provisioner privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
		new Test(provisioner2, password, 'getting an entry with provisioner privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(provisioner3, password, 'getting an entry with provisioner privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
		new Test(designer1, password, 'getting an entry with designer privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
		new Test(designer2, password, 'getting an entry with designer privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(designer3, password, 'getting an entry with designer privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
		new Test(cmpAdmin1, password, 'getting an entry with company Admin privileges. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
		new Test(cmpAdmin2, password, 'getting an entry with company Admin privileges from another company. Should succeed but return nothing',
			function(res){return check.OK_Eqls(res, {})}),
		new Test(cmpAdmin3, password, 'getting an entry with company Admin privileges from a parent company. Should succeed and return the expected contents',
			function(res){return check.OK_Eqls(res, cLACheckBody)}),
	]
	return check.permissions(function(){return authenticator.get(getEntryURL(cLATable), null, cLAGetQuery, true)}, testArray)
})

describe("show orphaned company level access entries", function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("cleaning orphans", function()
	{
		return authenticator.get(getEntryURL(cLATable), null, cLAGetQuery, true)
	})
})
*/

describe("test cALATable " + cALATable, function()
{
	before(function(){return authenticator.pLogin("tinokuchi", "tinokuchi")})
	after(function(){return authenticator.pLogout("tinokuchi")})
	it("test", function()
	{
		return authenticator.get(getEntryURL(cALATable), null, cALAGetQuery, true)
	})
})

describe("test cLATable " + cLATable, function()
{
	before(function(){return authenticator.pLogin(designer1, password)})
	after(function(){return authenticator.pLogout(designer1)})
	it("test", function()
	{
		return authenticator.get(getEntryURL(cLATable), null, cLAGetQuery, true)
	})
})


describe("test aLATable " + aLATable, function()
{
	before(function(){return authenticator.pLogin("tinokuchi", "tinokuchi")})
	after(function(){return authenticator.pLogout("tinokuchi")})
	it("test", function()
	{
		return authenticator.get(getEntryURL(aLATable), null, aLAGetQuery, true)
	})
})

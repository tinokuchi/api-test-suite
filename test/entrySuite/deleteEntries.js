var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const company2 = 'testCompany2'
const company3 = 'testCompany3'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'

//Templates for manipulating maps
const tableName = 'shippingRouteMapAssociations';
const getEntryURL = '/tables/' + tableName + '/entries';
const createEntryURL = '/tables/create/' + tableName;
const deleteEntryURL = '/tables';

const nonExistantTableName = 'thisTableNameDoesNotExist';

const companyId1 = 111;
const description = 'this is a description';

const fullName = "FullName";

const getFullQuery =
{
	where: "name='" + fullName + "'",
	field:["name","companyId","description"],
}
const checkFullBody=
{
	'0':
	{
		name: fullName,
		companyId: companyId1.toString(),
		description: description,
	}
}
const createFullBody =
{
	'name':{'type':'string', 'value':fullName},
	'companyId':{'type':'integer', 'value':companyId1},
	'description':{'type':'string', 'value':description},
}
const deleteFullBody =
{
	'table':tableName,
	'where':'name="' + fullName + '"',
}

/*
describe('delete orphaned entries', function()
{
	before(function(){authenticator.pLogin(designer1, password)})
	after(function(){authenticator.pLogout(designer1)})
	it('cleaning orphans', function()
	{
		return authenticator.delete(deleteEntryURL, deleteFullBody)
	})
})
*/

describe('delete an entry', function()
{
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createEntryURL, createFullBody)
			.then(function(res){return check.CREATED_ENTRYID(res)})
		})
	})
	after(function(){return authenticator.pLogout(designer1)})
	it('delete Entry, then getEntry to see if it still exists', function()
	{
		return authenticator.delete(deleteEntryURL, deleteFullBody)
		.then(function(res){return check.OK_DNE(res)})
		.then(function(){return authenticator.get(getEntryURL, null, getFullQuery)})
		.then(function(res){return check.OK_Eqls(res, {})})
	})
})


describe('delete with various privileges', function()
{
	//	The underlying ic call (icDeleteEntry I believe) is such that it will return
	//	0 RETURN_OK provided the user has access to the specified table, regardless
	//	of whether the user has access to the specific entry, or if that entry even
	//	exists. Verifying correct functionality thus entails first launching the delete request,
	//	then secondly logging in as a user with get privileges and checking if the entry still exists.

	//	Furthermore, regardless of whether tested user had or did not have the privileges to
	//	delete the entry, if the entry is deleted, it must be re-created for the next round of testing.
	//	"checkDelete()" thus serves two purposes: checking if the entry still exists and either
	//	rejecting or resolving a promise based on the deleter's privileges, and re-creating the entry
	//	if it isn't there.

	checkDelete = function(testUserHadPrivileges, doPrint=false)
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.get(getEntryURL, null, getFullQuery)
			.then(function(res)
			{
				return check.OK_Eqls(res, {}, doPrint)
				.then(function()
				{
					return authenticator.post(createEntryURL, createFullBody)
					.then(function(resInner){return check.CREATED_ENTRYID(resInner)})
					.finally(function()
					{
						if(testUserHadPrivileges)
							return Promise.resolve(res)
						else
							return check.OK_Eqls(res, checkFullBody)
					})
				}
				,function(error)
				{
					if(testUserHadPrivileges)
						return Promise.reject(error)
					else
						return check.OK_Eqls(res, checkFullBody)
				})
			})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	}
	testWrapper = function(testUserName, hasPrivileges, doPrint=false)
	{
		return check.permission(function(){return authenticator.delete(deleteEntryURL, deleteFullBody)}, testUserName, password, function(res){return check.OK_DNE(res)})
		.finally(function(){return checkDelete(hasPrivileges, doPrint)})
	}
	before(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.post(createEntryURL, createFullBody)
			.then(function(res){return check.CREATED_ENTRYID(res)})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	after(function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.delete(deleteEntryURL, deleteFullBody)
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
	it('deleting without logging in. Should fail with UNAUTHORIZED', function()
	{
		return authenticator.delete(deleteEntryURL, deleteFullBody)
		.then(function(res){return check.UNAUTHORIZED(res)})
	})
	it('deleting as a public user. Should succeed, but not delete the entry', function()
	{return testWrapper(public1,false)})
	it('deleting as a public user of another company. Should succeed, but not delete the entry', function()
	{return testWrapper(public2,false)})
	it('deleting as a public user of a parent company. Should succeed, but not delete the entry', function()
	{return testWrapper(public3,false)})
	it('deleting as a provisioner. Should succeed, but not delete the entry', function()
	{return testWrapper(provisioner1,false)})
	it('deleting as a provisioner of another company. Should succeed, but not delete the entry', function()
	{return testWrapper(provisioner2,false)})
	it('deleting as a provisioner of a parent company. Should succeed, but not delete the entry', function()
	{return testWrapper(provisioner3,false)})
	it('deleting as a designer. Should succeed', function()
	{return testWrapper(designer1,true)})
	it('deleting as a designer of another company. Should succeed, but not delete the entry', function()
	{return testWrapper(designer2,false)})
	it('deleting as a designer of a parent company. Should succeed', function()
	{return testWrapper(designer3,true)})
	it('deleting as a company Admin. Should succeed', function()
	{return testWrapper(cmpAdmin1,true)})
	it('deleting as a company Admin of another company. Should succeed, but not delete the entry', function()
	{return testWrapper(cmpAdmin2,false)})
	it('deleting as a company Admin of a parent company. Should succeed', function()
	{return testWrapper(cmpAdmin3,true)})
})

describe('delete from a nonexistant tableName', function()
{
	const deleteFakeFullBody =
	{
		'table':nonExistantTableName,
		'where':'name="' + fullName + '"',
	}
	it('Should fail with CONFLICT', function()
	{
		return authenticator.pLogin(designer1, password)
		.then(function()
		{
			return authenticator.delete(deleteEntryURL, deleteFakeFullBody)
			.then(function(res){return check.UNKNOWN_TABLE(res)})
			.finally(function(){return authenticator.pLogout(designer1)})
		})
	})
})

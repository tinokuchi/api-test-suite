/*
	As of Steve's 8/22/18 email, any table that isn't public privilege read accessible
	lacks a GUID column: Because getEntriesGeneric() requires a GUID, it's impossible
	for an unauthorized user to read from a cmpAdmin only table when supplying a
	public username, simply because the underlying SQL will reject the unneccessary.

	Need Steve to grant public access to testCompany2 112,
	then I can test getting company 1's maps from company 2.
*/
var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiSubset = require('chai-subset');
chai.use(chaiHttp);
chai.use(chaiSubset);
var expect = chai.expect;
var Promise = require('promise');
var {authenticator} = require('../common.js');
var {check} = require('../common.js');
var {Test} = require('../common.js');


//Test user information. All users share the same password.
//Login Information
//testCompany3 is a parent of testCompany1
//all test users share the same password
const company1 = 'testCompany1'
const companyId1 = '111'
const company2 = 'testCompany2'
const companyId2 = '112'
const company3 = 'testCompany3'
const companyId3 = '113'

const public1 = 'testUser_Public1'
const public2 = 'testUser_Public2'
const public3 = 'testUser_Public3'

const provisioner1 = 'testUser_Provisioner1'
const provisioner2 = 'testUser_Provisioner2'
const provisioner3 = 'testUser_Provisioner3'

const designer1 = 'testUser_Designer1'
const designer2 = 'testUser_Designer2'
const designer3 = 'testUser_Designer3'

const cmpAdmin1 = 'testUser_CmpAdmin1'
const cmpAdmin2 = 'testUser_CmpAdmin2'
const cmpAdmin3 = 'testUser_CmpAdmin3'

const password = '966LoggerIn'


//variables and functions for testing this request
const getEntriesGenericURL = "/tables/getEntriesGeneric"
const tableName = 'shippingPickList'
const nonExistantTableName = 'nonExistantTableName'
const nonGuidTableName = 'shippingRouteMapAssociations'

const entryGuid1 = '01AC6F49-8F0D-4154-BA2A-8421BB8F1B90'
const entryName1 = 'Job 09F85DE8 - Created Mar-19-2018'
const entryId1 = '2196'

const entryGuid2 = '8CDF7AB5-987B-4178-B7AA-49C4B27F377A'
const entryName2 = 'Job 09C9085A - Updated Feb-13-2018'
const entryId2 = '2195'

getQuery1 = function(userName)
{
	dataBody =
	{
		"username": userName,
		"guid": entryGuid1,
		"tableName": tableName,
		"field":['name','companyId','entryId'],
		"sort":'entryId desc',
	}
	return {"data":JSON.stringify(dataBody)}
}
getQuery2 = function(userName)
{
	dataBody =
	{
		"username": userName,
		"guid": entryGuid2,
		"tableName": tableName,
		"field":['name','companyId','entryId'],
		"sort":'entryId desc',
	}
	return {"data":JSON.stringify(dataBody)}
}
const checkBody1 =
{
	"0":
	{
		"name": entryName1,
		"companyId": companyId1,
		"entryId": entryId1,
	}
}
const checkBody2 =
{
	"0":
	{
		"name": entryName2,
		"companyId": companyId2,
		"entryId": entryId2,
	}
}

describe("Get a table entry using various usernames from a Generic Access company", function()
{
	wrapper = function(userName, itString)
	{
		it(itString, function()
		{
			return authenticator.get(getEntriesGenericURL, null, getQuery2(userName))
			.then(function(res){return check.OK_Eqls(res, checkBody2)})
		})
	}
	wrapper(public2, "Getting using a public user's username. Should succeed and return the expected entry")
	wrapper(provisioner2, "Getting using a provisioner's username. Should succeed and return the expected entry")
	wrapper(designer2, "Getting using a designer's username. Should succeed and return the expected entry")
	wrapper(cmpAdmin2, "Getting using a cmpAdmin's username. Should succeed and return the expected entry")
})

describe("Get a child's table entry using various usernames from a Generic Access company", function()
{
	wrapper = function(userName, itString)
	{
		it(itString, function()
		{
			return authenticator.get(getEntriesGenericURL, null, getQuery1(userName))
			.then(function(res){return check.OK_Eqls(res, checkBody1)})
		})
	}
	wrapper(public3, "Getting using a public user's username. Should succeed and return the expected entry")
	wrapper(provisioner3, "Getting using a provisioner's username. Should succeed and return the expected entry")
	wrapper(designer3, "Getting using a designer's username. Should succeed and return the expected entry")
	wrapper(cmpAdmin3, "Getting using a cmpAdmin's username. Should succeed and return the expected entry")
})

describe("Get a table entry while logged in from a non-Generic Access company, using a Public Access username", function()
{
	before(function(){return authenticator.pLogin(cmpAdmin1, password)})
	after(function(){return authenticator.pLogout(cmpAdmin1)})
	it("Should succeed and return the expected entry", function()
	{
		return authenticator.get(getEntriesGenericURL, null, getQuery1(cmpAdmin3))
		.then(function(res){return check.OK_Eqls(res, checkBody1)})
	})
})

describe("Get a table entry using a non-Generic Access username", function()
{
	it("Should fail with FORBIDDEN", function()
	{
		return authenticator.get(getEntriesGenericURL, null, getQuery1(cmpAdmin1))
		.then(function(res){return check.AUTHENTICATION_FAILED(res)})
	})
})

describe("Get a table entry with various bad parameters", function()
{
	it("Getting with a missing parameter. Should fail with BAD_REQUEST", function()
	{
		dataBody =
		{
			"username": cmpAdmin3,
			"guid": entryGuid1,
			"tableName": tableName,
			"field":['name','companyId','entryId'],
		}
		tempGetQuery = {"data":JSON.stringify(dataBody)}
		return authenticator.get(getEntriesGenericURL, null, tempGetQuery)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("Getting from a nonExistant table. Should fail with BAD_REQUEST", function()
	{
		dataBody =
		{
			"username": cmpAdmin3,
			"guid": entryGuid1,
			"tableName": nonExistantTableName,
			"field":['name','companyId','entryId'],
			"sort":'entryId desc',
		}
		tempGetQuery = {"data":JSON.stringify(dataBody)}
		return authenticator.get(getEntriesGenericURL, null, tempGetQuery)
		.then(function(res){return check.UNKNOWN_TABLE(res)})
	})
	it("Getting with a bad startRow. Should fail with BAD_REQUEST", function()
	{
		dataBody =
		{
			"username": cmpAdmin3,
			"guid": entryGuid1,
			"tableName": tableName,
			"field":['name','companyId','entryId'],
			"sort":'entryId desc',
			"startRow":-1000,
		}
		tempGetQuery = {"data":JSON.stringify(dataBody)}
		return authenticator.get(getEntriesGenericURL, null, tempGetQuery)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("Getting with a bad max. Should fail with BAD_REQUEST", function()
	{
		dataBody =
		{
			"username": cmpAdmin3,
			"guid": entryGuid1,
			"tableName": nonExistantTableName,
			"field":['name','companyId','entryId'],
			"sort":'entryId desc',
			"max":-1000,
		}
		tempGetQuery = {"data":JSON.stringify(dataBody)}
		return authenticator.get(getEntriesGenericURL, null, tempGetQuery)
		.then(function(res){return check.BAD_REQUEST(res)})
	})
	it("Getting from a table with no GUID. Should fail with INTERNAL_SERVER_ERROR", function()
	{
		dataBody =
		{
			"username": cmpAdmin3,
			"guid": entryGuid1,
			"tableName": nonGuidTableName,
			"field":['name','companyId','entryId'],
			"sort":'entryId desc',
		}
		tempGetQuery = {"data":JSON.stringify(dataBody)}
		return authenticator.get(getEntriesGenericURL, null, tempGetQuery)
		.then(function(res){return check.SQL_ERROR(res)})
	})
})
